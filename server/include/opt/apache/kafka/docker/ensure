#!/usr/bin/env bash
#
# Copyright 2024 Uwe Eisele
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. /opt/apache/kafka/docker/bash-config

export KAFKA_DATA_DIRS="${KAFKA_DATA_DIRS:-"/opt/apache/kafka/data"}"
echo "===> Check if $KAFKA_DATA_DIRS is writable ..."
GODUB_PATH_TIMEOUT="${GODUB_PATH_TIMEOUT:-0s}"
godub path --timeout "${GODUB_PATH_TIMEOUT}" -r -w "${KAFKA_DATA_DIRS}"

if [[ -n "${KAFKA_PROCESS_ROLES-}" ]]
then
    if ! kafka-storage.sh info -c /opt/apache/kafka/config/server.properties
    then
        echo "In KRaft mode metadata log dir must be formated before server is started (https://github.com/apache/kafka/blob/trunk/config/kraft/README.md)!"
        exit 1
    fi
fi
