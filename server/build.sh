#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
source "${SCRIPT_DIR}/env"
source "${SCRIPT_DIR}/../openjdk/env"

PUSH=false
BUILD=false

CONTAINERFILE=Containerfile
CONTAINERREGISTRY=docker.io
CONTAINERREGISTRYUSER="ueisele"

ARCHS=(amd64)

KAFKA_GITHUB_REPO="apache/kafka"
ZULU_OPENJDK_RELEASE=21

function usage() {
  echo "$0: $1" >&2
  echo
  echo "Usage: $0 [--build] [--push] [--registry docker.io] [--user ueisele] [--archs amd64,arm64] [--github-repo apache/kafka] [--commit-sha 60e8456] [--tag 3.9.0] [--branch trunk] [--pull-request 9999] [--openjdk-release 21] [--openjdk-version 21.0.5] [--patch 3.9.0-21.patch] [--cache-maven <m2-directory>] [--cache-gradle <gradle-directory>]"
  echo
  return 1
}

function assemble() {
  buildah bud \
    --target build \
    --layers=true \
    --userns host \
    $([ -n "${CACHE_MAVEN_DIR}" ] && echo --volume "${CACHE_MAVEN_DIR}:/home/appuser/.m2:z" || true) \
    $([ -n "${CACHE_GRADLE_DIR}" ] && echo --volume "${CACHE_GRADLE_DIR}:/home/appuser/.gradle:z" || true) \
    --build-arg "OPENJDK_JDK_IMAGE=$(openjdk_image_fqn "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}" jdk "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")" \
    --build-arg "OPENJDK_JRE_IMAGE=$(openjdk_micro_image_fqn "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}" jre "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")" \
    --build-arg "KAFKA_GIT_REPO=${KAFKA_GIT_REPO}" \
    --build-arg "KAFKA_GIT_REFSPEC=${KAFKA_GIT_COMMIT_SHA}" \
    --build-arg "KAFKA_BUILD_GIT_REFSPEC=${KAFKA_BUILD_GIT_REFSPEC}" \
    --build-arg "KAFKA_VERSION=${KAFKA_VERSION}" \
    --build-arg "KAFKA_PATCH=${KAFKA_PATCH}" \
    -f "${SCRIPT_DIR}/${CONTAINERFILE}" "${SCRIPT_DIR}"
}

function build_image() {
  local arch=${1:?"Missing arch as first parameter!"}
  local image=${2:?"Missing image name as second parameter!"}
  buildah bud \
    --platform "linux/${arch}" \
    --layers=true \
    --userns host \
    --build-arg "OPENJDK_JDK_IMAGE=$(openjdk_image_fqn "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}" jdk "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")" \
    --build-arg "OPENJDK_JRE_IMAGE=$(openjdk_micro_image_fqn "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}" jre "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")" \
    --build-arg "KAFKA_GIT_REPO=${KAFKA_GIT_REPO}" \
    --build-arg "KAFKA_GIT_REFSPEC=${KAFKA_GIT_COMMIT_SHA}" \
    --build-arg "KAFKA_BUILD_GIT_REFSPEC=${KAFKA_BUILD_GIT_REFSPEC}" \
    --build-arg "KAFKA_VERSION=${KAFKA_VERSION}" \
    --build-arg "KAFKA_PATCH=${KAFKA_PATCH}" \
    -t "${image}" \
    -f "${SCRIPT_DIR}/${CONTAINERFILE}" "${SCRIPT_DIR}"
}

function build() {
  local image
  image="$(kafka_server_image_name "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}" "${KAFKA_GITHUB_REPO}")"
  local tags=()
  IFS=" " read -r -a tags <<<"$(kafka_server_image_tags "${KAFKA_TAG_VERSION}" "$(echo "${KAFKA_GIT_COMMIT_SHA}" | cut -c 1-7)" "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")"
  echo "Assemble Kafka version ${KAFKA_VERSION} (${KAFKA_GIT_COMMIT_SHA}) from ${KAFKA_BUILD_GIT_REFSPEC}."
  assemble
  for arch in "${ARCHS[@]}"; do
    echo "Building Container image with Kafka version ${KAFKA_VERSION} (${KAFKA_GIT_COMMIT_SHA}) from ${KAFKA_BUILD_GIT_REFSPEC} for architecture ${arch}."
    local suffix
    if [ ${#ARCHS[@]} -gt 1 ]; then
      suffix="-${arch}"
    fi    
    local image_fqn="${image}${suffix}:${tags[0]}"
    build_image "${arch}" "${image_fqn}"
  done
}

function push() {
  echo "Pushing Container images with Kafka version ${KAFKA_VERSION} (${KAFKA_GIT_COMMIT_SHA}) from ${KAFKA_BUILD_GIT_REFSPEC}."
  local image
  image="$(kafka_server_image_name "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}" "${KAFKA_GITHUB_REPO}")"
  local tags=()
  IFS=" " read -r -a tags <<<"$(kafka_server_image_tags "${KAFKA_TAG_VERSION}" "$(echo "${KAFKA_GIT_COMMIT_SHA}" | cut -c 1-7)" "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")"
  tags+=("${tags[0]}-$(date +%Y%m%d%H%M)")
  local manifest_fqn="${image}:${tags[0]}"
  if [ ${#ARCHS[@]} -gt 1 ]; then
    buildah manifest rm "${manifest_fqn}" 1>/dev/null 2>&1 || true
    buildah rmi "${manifest_fqn}" 1>/dev/null 2>&1 || true
    buildah manifest create "${manifest_fqn}"
    for arch in "${ARCHS[@]}"; do
      local image_fqn="${image}-${arch}:${tags[0]}"
      buildah manifest add "${manifest_fqn}" "${image_fqn}"
    done
    for tag in "${tags[@]}"; do
      buildah manifest push --all "${manifest_fqn}" "docker://${image}:${tag}"
    done
  else
    for tag in "${tags[@]}"; do
      buildah push --all "${manifest_fqn}" "docker://${image}:${tag}"
    done
  fi
}

function resolveKafkaVersion() {
  local git_repo=${1:?"Missing Kafka Git repo as first parameter!"}
  local kafka_git_commit_sha=${2:-""}
  curl -s -L https://raw.githubusercontent.com/${git_repo}/${kafka_git_commit_sha}/gradle.properties | sed -n 's/^version=\(.\+\)$/\1/p'
}

function parseCmd() {
  while [[ $# -gt 0 ]]; do
    case "$1" in
    --build)
      BUILD=true
      shift
      ;;
    --push)
      PUSH=true
      shift
      ;;
    --registry)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Container registry name"
        return 1
        ;;
      *)
        CONTAINERREGISTRY="$1"
        shift
        ;;
      esac
      ;;
    --user)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Container registry user name"
        return 1
        ;;
      *)
        CONTAINERREGISTRYUSER="$1"
        shift
        ;;
      esac
      ;;
    --archs)
      shift
      case "$1" in
      "" | --*)
        usage "Requires comma-separated string of archs"
        return 1
        ;;
      *)
        IFS=, read -a ARCHS <<<"$1"
        shift
        ;;
      esac
      ;;
    --github-repo)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Kafka GitHub Repo"
        return 1
        ;;
      *)
        KAFKA_GITHUB_REPO="$1"
        shift
        ;;
      esac
      ;;
    --commit-sha)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Kafka Git Commit-Sha"
        return 1
        ;;
      *)
        KAFKA_GIT_COMMIT_SHA="$1"
        shift
        ;;
      esac
      ;;
    --tag)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Kafka Git Tag"
        return 1
        ;;
      *)
        KAFKA_GIT_TAG="$1"
        shift
        ;;
      esac
      ;;
    --branch)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Kafka Git Branch"
        return 1
        ;;
      *)
        KAFKA_GIT_BRANCH="$1"
        shift
        ;;
      esac
      ;;
    --pull-request)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Kafka Git pull request number"
        return 1
        ;;
      *)
        KAFKA_GIT_PULL_REQUEST="$1"
        shift
        ;;
      esac
      ;;
    --openjdk-release)
      shift
      case "$1" in
      "" | --*)
        usage "Requires OpenJDK release"
        return 1
        ;;
      *)
        ZULU_OPENJDK_RELEASE="$1"
        shift
        ;;
      esac
      ;;
    --openjdk-version)
      shift
      case "$1" in
      "" | --*)
        usage "Requires OpenJDK version"
        return 1
        ;;
      *)
        ZULU_OPENJDK_VERSION="$1"
        shift
        ;;
      esac
      ;;
    --patch)
      shift
      case "$1" in
      "" | --*)
        KAFKA_PATCH_DYNAMIC="true"
        unset KAFKA_PATCH
        ;;
      *)
        KAFKA_PATCH="$1"
        unset KAFKA_PATCH_DYNAMIC
        shift
        ;;
      esac
      ;;
    --cache-maven)
      shift
      case "$1" in
      "" | --*)
        CACHE_MAVEN_DIR="${HOME}/.m2"
        ;;
      *)
        CACHE_MAVEN_DIR="$1"
        shift
        ;;
      esac
      ;;
    --cache-gradle)
      shift
      case "$1" in
      "" | --*)
        CACHE_GRADLE_DIR="${HOME}/.gradle"
        ;;
      *)
        CACHE_GRADLE_DIR="$1"
        shift
        ;;
      esac
      ;;                
    *)
      local param="$1"
      shift
      case "$1" in
      "" | --*)
        echo "WARN: Unknown option: ${param}"
        ;;
      *)
        echo "WARN: Unknown option: ${param} $1"
        shift
        ;;
      esac
      ;;
    esac
  done

  KAFKA_GIT_REPO="https://github.com/${KAFKA_GITHUB_REPO}.git"

  if [ -n "${KAFKA_GIT_COMMIT_SHA}" ]; then
    KAFKA_BUILD_GIT_REFSPEC="commit/${KAFKA_GIT_COMMIT_SHA}"
    KAFKA_VERSION="$(resolveKafkaVersion ${KAFKA_GITHUB_REPO} ${KAFKA_GIT_COMMIT_SHA})"
    KAFKA_TAG_VERSION="${KAFKA_VERSION}-g$(echo ${KAFKA_GIT_COMMIT_SHA} | cut -c 1-7)"
  elif [ -n "${KAFKA_GIT_TAG}" ]; then
    KAFKA_GIT_COMMIT_SHA=$(git ls-remote --tags ${KAFKA_GIT_REPO} "refs/tags/${KAFKA_GIT_TAG}" | awk '{ print $1}')
    KAFKA_BUILD_GIT_REFSPEC="tags/${KAFKA_GIT_TAG}"
    KAFKA_VERSION=${KAFKA_GIT_TAG}
    KAFKA_TAG_VERSION=${KAFKA_GIT_TAG}
  elif [ -n "${KAFKA_GIT_BRANCH}" ]; then
    KAFKA_GIT_COMMIT_SHA=$(git ls-remote --heads ${KAFKA_GIT_REPO} refs/heads/${KAFKA_GIT_BRANCH} | awk '{ print $1}')
    KAFKA_BUILD_GIT_REFSPEC="heads/${KAFKA_GIT_BRANCH}"
    KAFKA_VERSION="$(resolveKafkaVersion ${KAFKA_GITHUB_REPO} ${KAFKA_GIT_COMMIT_SHA})"
    if [ "${KAFKA_GIT_BRANCH}" == "trunk" ] || [[ "${KAFKA_VERSION}" =~ ^${KAFKA_GIT_BRANCH} ]]; then
      KAFKA_TAG_VERSION="${KAFKA_VERSION}"
    else
      KAFKA_TAG_VERSION="${KAFKA_GIT_BRANCH}"
    fi
  elif [ -n "${KAFKA_GIT_PULL_REQUEST}" ]; then
    KAFKA_GIT_COMMIT_SHA=$(git ls-remote --refs ${KAFKA_GIT_REPO} refs/pull/${KAFKA_GIT_PULL_REQUEST}/head | awk '{ print $1}')
    KAFKA_BUILD_GIT_REFSPEC="pull/${KAFKA_GIT_PULL_REQUEST}"
    KAFKA_VERSION="$(resolveKafkaVersion ${KAFKA_GITHUB_REPO} ${KAFKA_GIT_COMMIT_SHA})"
    KAFKA_TAG_VERSION="${KAFKA_BUILD_GIT_REFSPEC//\//}"
  fi

  if [ -z "${KAFKA_VERSION}" ] || [ -z "${KAFKA_GIT_COMMIT_SHA}" ]; then
    usage "commit-sha, tag, branch or pull-request is invalid"
    return 1
  fi

  if [ -z "${ZULU_OPENJDK_VERSION}" ]; then
    ZULU_OPENJDK_VERSION="$(openjdk_version_by_release "${ZULU_OPENJDK_RELEASE}")"
    if [ -z "${ZULU_OPENJDK_VERSION}" ]; then
      usage "requires OpenJDK version"
      return 1
    fi
  fi

  if [ "${KAFKA_PATCH_DYNAMIC}" == "true" ] && [ -e "${SCRIPT_DIR}/patch/${KAFKA_VERSION}-${ZULU_OPENJDK_RELEASE}.patch" ]; then
    KAFKA_PATCH="${KAFKA_VERSION}-${ZULU_OPENJDK_RELEASE}.patch"
  fi

  if [ -n "${KAFKA_PATCH}" ] && [ ! -e "${SCRIPT_DIR}/patch/${KAFKA_PATCH}" ]; then
    usage "missing patch file ${SCRIPT_DIR}/patch/${KAFKA_PATCH}"
    return 1
  fi

  if [ -n "${CACHE_MAVEN_DIR}" ]; then
    if ! [ -e "${CACHE_MAVEN_DIR}" ]; then
      mkdir -p "${CACHE_MAVEN_DIR}"
      chmod -R ugo+rwX "${CACHE_MAVEN_DIR}"
    fi
    CACHE_MAVEN_DIR="$(realpath "$CACHE_MAVEN_DIR")"
    echo "Using '${CACHE_MAVEN_DIR}' as Maven cache."
  fi
  if [ -n "${CACHE_GRADLE_DIR}" ]; then
    if ! [ -e "${CACHE_GRADLE_DIR}" ]; then
      mkdir -p "${CACHE_GRADLE_DIR}"
      chmod -R ugo+rwX "${CACHE_GRADLE_DIR}"
    fi
    CACHE_GRADLE_DIR="$(realpath "$CACHE_GRADLE_DIR")"
    echo "Using '${CACHE_GRADLE_DIR}' as Gradle cache."
  fi

  return 0
}

function main() {
  parseCmd "$@"
  local retval=$?
  if [ $retval != 0 ]; then
    exit $retval
  fi

  if [ "$BUILD" = true ]; then
    build
  fi
  if [ "$PUSH" = true ]; then
    push
  fi
}

main "$@"
