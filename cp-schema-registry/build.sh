#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
source "${SCRIPT_DIR}/env"
source "${SCRIPT_DIR}/../openjdk/env"

BUILD=false
PUSH=false

CONTAINERFILE=Containerfile
CONTAINERREGISTRY=docker.io
CONTAINERREGISTRYUSER="ueisele"

ARCHS=(amd64)

ZULU_OPENJDK_RELEASE=21

function usage() {
  echo "$0: $1" >&2
  echo
  echo "Usage: $0 --version 7.8.0 [--build] [--push] [--registry docker.io] [--user ueisele] [--archs amd64,arm64] [--openjdk-release 21] [--openjdk-version 21.0.5] [--cache-maven <m2-directory>]"
  echo
  return 1
}

function assemble() {
  buildah bud \
    --target build \
    --layers=true \
    $([ -n "${CACHE_MAVEN_DIR}" ] && echo --volume "${CACHE_MAVEN_DIR}:/home/appuser/.m2:z" || true) \
    --build-arg "OPENJDK_JDK_IMAGE=$(openjdk_image_fqn "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}" jdk "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")" \
    --build-arg "CP_SCHEMA_REGISTRY_VERSION=${CP_SCHEMA_REGISTRY_VERSION}" \
    -f "${SCRIPT_DIR}/${CONTAINERFILE}" "${SCRIPT_DIR}"
}

function build_image() {
  local arch=${1:?"Missing arch as first parameter!"}
  local image=${2:?"Missing image name as second parameter!"}
  buildah bud \
    --platform "linux/${arch}" \
    --layers=true \
    --build-arg "OPENJDK_JDK_IMAGE=$(openjdk_image_fqn "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}" jdk "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")" \
    --build-arg "OPENJDK_JRE_IMAGE=$(openjdk_micro_image_fqn "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}" jre "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")" \
    --build-arg "CP_SCHEMA_REGISTRY_VERSION=${CP_SCHEMA_REGISTRY_VERSION}" \
    -t "${image}" \
    -f "${SCRIPT_DIR}/${CONTAINERFILE}" "${SCRIPT_DIR}"
}

function build() {
  local image
  image="$(cp_schema_registry_image_name "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}")"
  local tags=()
  IFS=" " read -r -a tags <<<"$(cp_schema_registry_image_tags "${CP_SCHEMA_REGISTRY_VERSION}" "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")"
  echo "Assemble CP Schema Registry version ${CP_SCHEMA_REGISTRY_VERSION}."
  assemble
  for arch in "${ARCHS[@]}"; do
    echo "Building Container image with CP Schema Registry version ${CP_SCHEMA_REGISTRY_VERSION} for architecture ${arch}."
    local suffix
    if [ ${#ARCHS[@]} -gt 1 ]; then
      suffix="-${arch}"
    fi    
    local image_fqn="${image}${suffix}:${tags[0]}"
    build_image "${arch}" "${image_fqn}"
  done
}

function push() {
  echo "Pushing Container images with CP Schema Registry version ${CP_SCHEMA_REGISTRY_VERSION}."
  local image
  image="$(cp_schema_registry_image_name "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}")"
  local tags=()
  IFS=" " read -r -a tags <<<"$(cp_schema_registry_image_tags "${CP_SCHEMA_REGISTRY_VERSION}" "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")"
  tags+=("${tags[0]}-$(date +%Y%m%d%H%M)")
  local manifest_fqn="${image}:${tags[0]}"
  if [ ${#ARCHS[@]} -gt 1 ]; then
    buildah manifest rm "${manifest_fqn}" 1>/dev/null 2>&1 || true
    buildah rmi "${manifest_fqn}" 1>/dev/null 2>&1 || true
    buildah manifest create "${manifest_fqn}"
    for arch in "${ARCHS[@]}"; do
      local image_fqn="${image}-${arch}:${tags[0]}"
      buildah manifest add "${manifest_fqn}" "${image_fqn}"
    done
    for tag in "${tags[@]}"; do
      buildah manifest push --all "${manifest_fqn}" "docker://${image}:${tag}"
    done
  else
    for tag in "${tags[@]}"; do
      buildah push --all "${manifest_fqn}" "docker://${image}:${tag}"
    done
  fi
}

function parseCmd() {
  while [[ $# -gt 0 ]]; do
    case "$1" in
    --build)
      BUILD=true
      shift
      ;;
    --push)
      PUSH=true
      shift
      ;;
    --registry)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Container registry name"
        return 1
        ;;
      *)
        CONTAINERREGISTRY="$1"
        shift
        ;;
      esac
      ;;
    --user)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Container registry user name"
        return 1
        ;;
      *)
        CONTAINERREGISTRYUSER="$1"
        shift
        ;;
      esac
      ;;
    --archs)
      shift
      case "$1" in
      "" | --*)
        usage "Requires comma-separated string of archs"
        return 1
        ;;
      *)
        IFS=, read -a ARCHS <<<"$1"
        shift
        ;;
      esac
      ;;
    --version)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Schema Registry version"
        return 1
        ;;
      *)
        CP_SCHEMA_REGISTRY_VERSION="$1"
        shift
        ;;
      esac
      ;;
    --openjdk-release)
      shift
      case "$1" in
      "" | --*)
        usage "Requires OpenJDK release"
        return 1
        ;;
      *)
        ZULU_OPENJDK_RELEASE="$1"
        shift
        ;;
      esac
      ;;
    --openjdk-version)
      shift
      case "$1" in
      "" | --*)
        usage "Requires OpenJDK version"
        return 1
        ;;
      *)
        ZULU_OPENJDK_VERSION="$1"
        shift
        ;;
      esac
      ;;
    --cache-maven)
      shift
      case "$1" in
      "" | --*)
        CACHE_MAVEN_DIR="${HOME}/.m2"
        ;;
      *)
        CACHE_MAVEN_DIR="$(realpath "$1")"
        shift
        ;;
      esac
      ;;      
    *)
      local param="$1"
      shift
      case "$1" in
      "" | --*)
        echo "WARN: Unknown option: ${param}"
        ;;
      *)
        echo "WARN: Unknown option: ${param} $1"
        shift
        ;;
      esac
      ;;
    esac
  done

  if [ -z "${CP_SCHEMA_REGISTRY_VERSION}" ]; then
    usage "version is required"
    return 1
  fi

  if [ -z "${ZULU_OPENJDK_VERSION}" ]; then
    ZULU_OPENJDK_VERSION="$(openjdk_version_by_release "${ZULU_OPENJDK_RELEASE}")"
    if [ -z "${ZULU_OPENJDK_VERSION}" ]; then
      usage "requires OpenJDK version"
      return 1
    fi
  fi

  if [ -n "${CACHE_MAVEN_DIR}" ]; then
    if ! [ -e "${CACHE_MAVEN_DIR}" ]; then
      mkdir -p "${CACHE_MAVEN_DIR}"
      chmod o+rwX -R "${CACHE_MAVEN_DIR}"
    fi
    echo "Using '${CACHE_MAVEN_DIR}' as Maven cache."
  fi

  return 0
}

function main() {
  parseCmd "$@"
  local retval=$?
  if [ $retval != 0 ]; then
    exit $retval
  fi

  if [ "$BUILD" = true ]; then
    build
  fi
  if [ "$PUSH" = true ]; then
    push
  fi
}

main "$@"
