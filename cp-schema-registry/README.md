# Container Image for Confluent Schema Registry

Container image for running the [Open Source version of Confluent Schema Registry](https://github.com/confluentinc/schema-registry).

The Confluent Schema Registry distribution included in the Container image is assembled via _maven-assembly-plugin_.

The Container images are available on Docker Hub repository [ueisele/cp-schema-registry](https://hub.docker.com/repository/docker/ueisele/cp-schema-registry), and the source files for the images are available on GitLab repository [ueisele/kafka-images](https://gitlab.com/ueisele/kafka-images).

## Most Recent Tags

Most recent tags for `RELEASE` builds:

* `7.8.0`, `7.8.0-zulu21`, `7.8.0-zulu21`, `7.8.0-zulu21-alma9.5`, `7.8.0-zulu21-alma9.5-20241118`

## Image

The Container images are based on [ueisele/zulu-openjdk-micro](https://hub.docker.com/repository/docker/ueisele/zulu-openjdk-micro) with _JRE_ installed (e.g. _21-jre_). 

The OpenJDK image in turn is based on [AlmaLinux 9 Micro](https://hub.docker.com/r/almalinux/9-micro).

As OpenJDK [Azul Zulu](https://www.azul.com/downloads/?package=jdk) is used.
Azul Zulu builds of OpenJDK are fully tested and TCK compliant builds of OpenJDK.

## Quick Start

In the following section you find some simple examples to run Confluent Schema Registry.

First create a Container network:
```bash
podman network create quickstart-cp-schema-registry
```

Now, start a single Kafka instance: 

```bash
podman run -d --name kafka --net quickstart-cp-schema-registry -p 9092:9092 docker.io/ueisele/apache-kafka-server-standalone:3.9.0
```

In order to run Confluent Schema Registry with a single instance run the following command:

```bash
podman run -d --name schema-registry --net quickstart-cp-schema-registry -p 8081:8081 -p 9464:9464 \
    -e SCHEMA_REGISTRY_HOST_NAME=schema-registry \
    -e SCHEMA_REGISTRY_LISTENERS=http://0.0.0.0:8081 \
    -e SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS=kafka:9092 \
    -e SCHEMA_REGISTRY_KAFKASTORE_TOPIC_REPLICATION_FACTOR=1 \
    docker.io/ueisele/cp-schema-registry:7.8.0
```

## Configuration

For the Confluent Schema Registry ([ueisele/cp-schema-registry](https://hub.docker.com/repository/registry-1.docker.io/ueisele/cp-schema-registry/)) image, convert the [Confluent Schema Registry configuration properties](https://docs.confluent.io/platform/current/schema-registry/installation/config.html) as below and use them as environment variables:

* Prefix with CONNECT_.
* Convert to upper-case.
* Replace a period (.) with a single underscore (_).
* Replace a dash (-) with double underscores (__).
* Replace an underscore (_) with triple underscores (___).

The configuration is fully compatible with the [Confluent Docker images](https://docs.confluent.io/platform/current/installation/docker/config-reference.html#sr-long-configuration).

The configuration mechanism supports [`Go Template`](https://pkg.go.dev/text/template) for environment variable values.
The templating is done by [`godub`](https://github.com/ueisele/go-docker-utils) and therefore provides its [template functions](https://github.com/ueisele/go-docker-utils#template-functions). 

### Required Configuration

The minimum required configuration is:

* `SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS` which defines a list of Kafka brokers to connect to.
* `SCHEMA_REGISTRY_HOST_NAME` is required if if you are running Schema Registry with multiple nodes. Hostname is required because it defaults to the Java canonical hostname for the container, which may not always be resolvable in a Container environment. Hostname must be resolveable because secondary nodes serve registration requests indirectly by simply forwarding them to the current primary, and returning the response supplied by the primary.

### Logging

The logging configuration can be adjusted with the following environment variables:

* `SCHEMA_REGISTRY_LOG4J_PATTERN` sets the logging pattern (default: `[%d] (%t) %p %m (%c)%n`)
* `SCHEMA_REGISTRY_LOG4J_ROOT_LOGLEVEL` sets the root log level (default: `INFO`)
* `SCHEMA_REGISTRY_LOG4J_LOGGERS` is a comma separated list of logger and log level key-value pairs (default: `kafka=ERROR,org.apache.kafka=ERROR,org.apache.zookeeper=ERROR`)

### JMX

Remote JMX can be enabled with the following environment variables:

```properties
SCHEMA_REGISTRY_JMX_PORT=6001
SCHEMA_REGISTRY_JMX_HOSTNAME=localhost
```

### Prometheus

The image contains the [Prometheus JMX Exporter JavaAgent](https://github.com/prometheus/jmx_exporter). In the default configuration, this exports metrics via a [Prometheus](https://prometheus.io/) endpoint on port _9464_.
As configuration for the Kafka metrics, the file [default.yaml](https://gitlab.com/ueisele/kafka-images/-/blob/main/cp-schema-registry/include/opt/confluent/schema-registry/javaagents/prometheus/configs/default.yaml) is used. 

The Prometheus JMX Exporter JavaAgent can be configured via environment variables:

> #### `PROMETHEUS_JAVAAGENT_ENABLED`
> Can be used to disable the Prometheus JMX Exporter JavaAgent.
> *   Type: `Boolean`
> *   Default: `true`
>
> #### `PROMETHEUS_EXPORTER_CONFIG`
> To provide your own metric definitions, create a [YAML configuration file](https://github.com/prometheus/jmx_exporter), and specify their location.
> The provided [default.yaml](https://gitlab.com/ueisele/kafka-images/-/blob/main/cp-schema-registry/include/opt/confluent/schema-registry/javaagents/prometheus/configs/default.yaml) can be used as an example.
> *   Type: `String`
> *   Default: `/opt/confluent/schema-registry/javaagents/prometheus/configs/default.yaml`
>
> #### `PROMETHEUS_EXPORTER_PORT`
> The port on which the prometheus metrics are provided.
> *   Type: `Integer`
> *   Default: `9464`
>

### Debugging

In order to debug Confluent Schema Registry, set the following environment variable:

```properties
SCHEMA_REGISTRY_DEBUG=y
```

In addition you can configure the behavior with the following environment variables:

```properties
JAVA_DEBUG_PORT=5005
DEBUG_SUSPEND_FLAG=y
```

## Build

In order to create your own Container image for Confluent Schema Registry clone the [ueisele/kafka-image](https://gitlab.com/ueisele/kafka-images) Git repository and run the build command:

```bash
git clone https://gitlab.com/ueisele/kafka-images.git
cd kafka-images
cp-schema-registry/build.sh --build --version 7.8.0 --openjdk-release 21
```

To create an image with a specific OpenJDK version use the following command:

```bash
cp-schema-registry/build.sh --build --tag 7.8.0 --openjdk-release 21 --openjdk-version 21.0.5
```

### Build Options

The `cp-schema-registry/build.sh` script provides the following options:

`Usage: cp-schema-registry/build.sh --version 7.8.0 [--build] [--push] [--registry docker.io] [--user ueisele] [--archs amd64,arm64] [--openjdk-release 21] [--openjdk-version 21.0.5] [--cache-maven <m2-directory>]`

## License 

This Container image is licensed under the [Apache 2 license](https://gitlab.com/ueisele/kafka-images/-/blob/main/LICENSE).