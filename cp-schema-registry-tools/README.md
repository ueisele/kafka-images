# Container Image for Confluent Schema Registry Tools

Container image for running the [Open Source version of Confluent Schema Registry Tools](https://github.com/confluentinc/schema-registry).

The Confluent Schema Registry Tools distribution included in the Container image is assembled via _maven-assembly-plugin_.

The Container images are available on Docker Hub repository [ueisele/cp-schema-registry-tools](https://hub.docker.com/repository/docker/ueisele/cp-schema-registry-tools), and the source files for the images are available on GitLab repository [ueisele/kafka-images](https://gitlab.com/ueisele/kafka-images).

## Most Recent Tags

Most recent tags for `RELEASE` builds:

* `7.8.0`, `7.8.0-zulu21`, `7.8.0-zulu21`, `7.8.0-zulu21-alma9.5`, `7.8.0-zulu21-alma9.5-20241118`

## Image

The Container images are based on [ueisele/zulu-openjdk-micro](https://hub.docker.com/repository/docker/ueisele/zulu-openjdk-micro) with _JRE_ installed (e.g. _21-jre_). 

The OpenJDK image in turn is based on [AlmaLinux 9 Micro](https://hub.docker.com/r/almalinux/9-micro).

As OpenJDK [Azul Zulu](https://www.azul.com/downloads/?package=jdk) is used.
Azul Zulu builds of OpenJDK are fully tested and TCK compliant builds of OpenJDK.

## Examples

For an example see [examples/connect-standalone/source-http-avro](../examples/connect-standalone/source-http-avro/).

## Build

In order to create your own Container image for Confluent Schema Registry clone the [ueisele/kafka-image](https://gitlab.com/ueisele/kafka-images) Git repository and run the build command:

```bash
git clone https://gitlab.com/ueisele/kafka-images.git
cd kafka-images
cp-schema-registry-tools/build.sh --build --version 7.8.0 --openjdk-release 21
```

To create an image with a specific OpenJDK version use the following command:

```bash
cp-schema-registry-tools/build.sh --build --tag 7.8.0 --openjdk-release 21 --openjdk-version 21.0.5
```

### Build Options

The `cp-schema-registry-tools/build.sh` script provides the following options:

`Usage: cp-schema-registry-tools/build.sh --version 7.8.0 [--build] [--push] [--registry docker.io] [--user ueisele] [--archs amd64,arm64] [--openjdk-release 21] [--openjdk-version 21.0.5] [--cache-maven <m2-directory>]`

## License 

This Container image is licensed under the [Apache 2 license](https://gitlab.com/ueisele/kafka-images/-/blob/main/LICENSE).