#############################################################
# Kafka Cluster with ZooKeeper                              #
# 3x ZooKeeper + 3x broker                                  #
#############################################################
---
services:
    
    #############################################################
    # Zookeeper                                                 #
    #############################################################
    zookeeper1:
        image: ${CONTAINER_REGISTRY}zookeeper:${ZOOKEEPER_VERSION}
        restart: always
        hostname: zookeeper1
        ports:
            - 12181:2181
            - 18080:8080
            - 17070:7070
        volumes:
            - zookeeper1_data:/data
            - zookeeper1_datalog:/datalog
        # https://hub.docker.com/_/zookeeper
        environment:
            ZOO_MY_ID: 1 # (required in cluster mode) Sets the server ID in the myid file, which consists of a single line that contains only the text of that machine’s ID.
            ZOO_SERVERS: server.1=zookeeper1:2888:3888;2181 server.2=zookeeper2:2888:3888;2181 server.3=zookeeper3:2888:3888;2181
            ZOO_TICK_TIME: 2000 # (default: 3000) The unit of time for ZooKeeper translated to milliseconds. This governs all ZooKeeper time dependent operations. It is used for heartbeats and timeouts especially.
            # The initLimit and syncLimit are used to govern how long following ZooKeeper servers can take to initialize with the current leader and how long they can be out of sync with the leader.
            ZOO_INIT_LIMIT: 5 # (default: 10)
            ZOO_SYNC_LIMIT: 2 # (default: 5)
            # Four letter words
            ZOO_4LW_COMMANDS_WHITELIST: "*" #(default: srvr) A list of comma separated Four Letter Words commands that user wants to use.
            ZOO_ADMINSERVER_ENABLED: "true" #(default: true) The server is started on port 8080, and commands are issued by going to the URL "/commands/[command name]"
            # Metrics
            ZOO_CFG_EXTRA: "metricsProvider.className=org.apache.zookeeper.metrics.prometheus.PrometheusMetricsProvider metricsProvider.httpPort=7070 metricsProvider.exportJvmInfo=true"

    zookeeper2:
        image: ${CONTAINER_REGISTRY}zookeeper:${ZOOKEEPER_VERSION}
        restart: always
        hostname: zookeeper2
        ports:
            - 22181:2181
            - 28080:8080
            - 27070:7070
        volumes:
            - zookeeper2_data:/data
            - zookeeper2_datalog:/datalog
        # https://hub.docker.com/_/zookeeper
        environment:
            ZOO_MY_ID: 2 # (required in cluster mode) Sets the server ID in the myid file, which consists of a single line that contains only the text of that machine’s ID.
            ZOO_SERVERS: server.1=zookeeper1:2888:3888;2181 server.2=zookeeper2:2888:3888;2181 server.3=zookeeper3:2888:3888;2181
            ZOO_INIT_LIMIT: 5 # (default: 10)
            ZOO_SYNC_LIMIT: 2 # (default: 5)
            # Four letter words
            ZOO_4LW_COMMANDS_WHITELIST: "*" #(default: srvr) A list of comma separated Four Letter Words commands that user wants to use.
            ZOO_ADMINSERVER_ENABLED: "true" #(default: true) The server is started on port 8080, and commands are issued by going to the URL "/commands/[command name]"
            # Metrics
            ZOO_CFG_EXTRA: "metricsProvider.className=org.apache.zookeeper.metrics.prometheus.PrometheusMetricsProvider metricsProvider.httpPort=7070 metricsProvider.exportJvmInfo=true"

    zookeeper3:
        image: ${CONTAINER_REGISTRY}zookeeper:${ZOOKEEPER_VERSION}
        restart: always
        hostname: zookeeper3
        ports:
            - 32181:2181
            - 38080:8080
            - 37070:7070
        volumes:
            - zookeeper3_data:/data
            - zookeeper3_datalog:/datalog
        # https://hub.docker.com/_/zookeeper
        environment:
            ZOO_MY_ID: 3 # (required in cluster mode) Sets the server ID in the myid file, which consists of a single line that contains only the text of that machine’s ID.
            ZOO_SERVERS: server.1=zookeeper1:2888:3888;2181 server.2=zookeeper2:2888:3888;2181 server.3=zookeeper3:2888:3888;2181
            ZOOKEEPER_TICK_TIME: 2000 # (default: 3000) The unit of time for ZooKeeper translated to milliseconds. This governs all ZooKeeper time dependent operations. It is used for heartbeats and timeouts especially.
            # The initLimit and syncLimit are used to govern how long following ZooKeeper servers can take to initialize with the current leader and how long they can be out of sync with the leader.
            ZOO_INIT_LIMIT: 5 # (default: 10)
            ZOO_SYNC_LIMIT: 2 # (default: 5)
            # Four letter words
            ZOO_4LW_COMMANDS_WHITELIST: "*" #(default: srvr) A list of comma separated Four Letter Words commands that user wants to use.
            ZOO_ADMINSERVER_ENABLED: "true" #(default: true) The server is started on port 8080, and commands are issued by going to the URL "/commands/[command name]"
            # Metrics
            ZOO_CFG_EXTRA: "metricsProvider.className=org.apache.zookeeper.metrics.prometheus.PrometheusMetricsProvider metricsProvider.httpPort=7070 metricsProvider.exportJvmInfo=true"


    #############################################################
    # Confluent Server                                          #
    #############################################################
    kafka-broker1:
        image: ${CONTAINER_REGISTRY}confluentinc/cp-server:${CONFLUENT_VERSION}
        restart: always
        ports:
            - 9092:19092
            - 19092:19092
        networks:
            default:
                aliases:
                    - kafka
        volumes:
            - kafka-broker1:/var/lib/kafka/data
        environment:
            #### Server Basics ####
            KAFKA_NODE_ID: 101 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_ZOOKEEPER_CONNECT: zookeeper1:2181,zookeeper2:2181,zookeeper3:2181 # (required) Instructs Kafka how to get in touch with ZooKeeper.
            #### Socket Server Settings ###
            KAFKA_ADVERTISED_LISTENERS: PLAINTEXT_HOST://localhost:19092,PLAINTEXT://kafka-broker1:9092 # (required) Describes how the host name that is advertised and can be reached by clients. PLAINTEXT_HOST://localhost:19092 enables access from Docker host.
            KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT_HOST:PLAINTEXT,PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT,SASL_SSL:SASL_SSL # (default: PLAINTEXT:PLAINTEXT,...) Map between listener names and security protocols. In this scenario this setting is used to define listeners with names.
            #### Defaults ####
            KAFKA_DEFAULT_REPLICATION_FACTOR: 3
            KAFKA_NUM_PARTITIONS: 12
            #### MD5 ####
            KAFKA_CONFLUENT_METADATA_SERVER_LISTENERS: http://0.0.0.0:8091
            KAFKA_CONFLUENT_METADATA_SERVER_ADVERTISED_LISTENERS: http://kafka-broker1:8091
            #### Metrics ####
            KAFKA_METRIC_REPORTERS: io.confluent.metrics.reporter.ConfluentMetricsReporter
            KAFKA_CONFLUENT_METRICS_REPORTER_BOOTSTRAP_SERVERS: kafka-broker1:9092
            KAFKA_CONFLUENT_METRICS_ENABLE: 'true'
            KAFKA_CONFLUENT_SUPPORT_CUSTOMER_ID: anonymous
            #### Self-Balancing Cluster (https://docs.confluent.io/platform/current/kafka/sbc/index.html) ####
            KAFKA_CONFLUENT_BALANCER_ENABLE: 'true'
            KAFKA_CONFLUENT_BALANCER_HEAL_UNEVEN_LOAD_TRIGGER: ANY_UNEVEN_LOAD
            KAFKA_CONFLUENT_BALANCER_HEAL_BROKER_FAILURE_THRESHOLD_MS: 900000
            KAFKA_CONFLUENT_REPORTERS_TELEMETRY_AUTO_ENABLE: 'true'

    kafka-broker2:
        image: ${CONTAINER_REGISTRY}confluentinc/cp-server:${CONFLUENT_VERSION}
        restart: always
        ports:
            - 29092:29092
        networks:
            default:
                aliases:
                    - kafka
        volumes:
            - kafka-broker2:/var/lib/kafka/data
        environment:
            #### Server Basics ####
            KAFKA_NODE_ID: 102 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_ZOOKEEPER_CONNECT: zookeeper1:2181,zookeeper2:2181,zookeeper3:2181 # (required) Instructs Kafka how to get in touch with ZooKeeper.
            #### Socket Server Settings ###
            KAFKA_ADVERTISED_LISTENERS: PLAINTEXT_HOST://localhost:29092,PLAINTEXT://kafka-broker2:9092 # (required) Describes how the host name that is advertised and can be reached by clients. PLAINTEXT_HOST://localhost:19092 enables access from Docker host.
            KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT_HOST:PLAINTEXT,PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT,SASL_SSL:SASL_SSL # (default: PLAINTEXT:PLAINTEXT,...) Map between listener names and security protocols. In this scenario this setting is used to define listeners with names.
            #### Defaults ####
            KAFKA_DEFAULT_REPLICATION_FACTOR: 3
            KAFKA_NUM_PARTITIONS: 12
            #### MD5 ####
            KAFKA_CONFLUENT_METADATA_SERVER_LISTENERS: http://0.0.0.0:8091
            KAFKA_CONFLUENT_METADATA_SERVER_ADVERTISED_LISTENERS: http://kafka-broker2:8091
            #### Metrics ####
            KAFKA_METRIC_REPORTERS: io.confluent.metrics.reporter.ConfluentMetricsReporter
            KAFKA_CONFLUENT_METRICS_REPORTER_BOOTSTRAP_SERVERS: kafka-broker2:9092
            KAFKA_CONFLUENT_METRICS_ENABLE: 'true'
            KAFKA_CONFLUENT_SUPPORT_CUSTOMER_ID: anonymous
            #### Self-Balancing Cluster (https://docs.confluent.io/platform/current/kafka/sbc/index.html) ####
            KAFKA_CONFLUENT_BALANCER_ENABLE: 'true'
            KAFKA_CONFLUENT_BALANCER_HEAL_UNEVEN_LOAD_TRIGGER: ANY_UNEVEN_LOAD
            KAFKA_CONFLUENT_BALANCER_HEAL_BROKER_FAILURE_THRESHOLD_MS: 900000
            KAFKA_CONFLUENT_REPORTERS_TELEMETRY_AUTO_ENABLE: 'true'

    kafka-broker3:
        image: ${CONTAINER_REGISTRY}confluentinc/cp-server:${CONFLUENT_VERSION}
        restart: always
        ports:
            - 39092:39092
        networks:
            default:
                aliases:
                    - kafka
        volumes:
            - kafka-broker3:/var/lib/kafka/data
        environment:
            #### Server Basics ####
            KAFKA_NODE_ID: 103 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_ZOOKEEPER_CONNECT: zookeeper1:2181,zookeeper2:2181,zookeeper3:2181 # (required) Instructs Kafka how to get in touch with ZooKeeper.
            #### Socket Server Settings ###
            KAFKA_ADVERTISED_LISTENERS: PLAINTEXT_HOST://localhost:39092,PLAINTEXT://kafka-broker3:9092 # (required) Describes how the host name that is advertised and can be reached by clients. PLAINTEXT_HOST://localhost:19092 enables access from Docker host.
            KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT_HOST:PLAINTEXT,PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT,SASL_SSL:SASL_SSL # (default: PLAINTEXT:PLAINTEXT,...) Map between listener names and security protocols. In this scenario this setting is used to define listeners with names.
            #### Defaults ####
            KAFKA_DEFAULT_REPLICATION_FACTOR: 3
            KAFKA_NUM_PARTITIONS: 12
            #### MD5 ####
            KAFKA_CONFLUENT_METADATA_SERVER_LISTENERS: http://0.0.0.0:8091
            KAFKA_CONFLUENT_METADATA_SERVER_ADVERTISED_LISTENERS: http://kafka-broker3:8091
            #### Metrics ####
            KAFKA_METRIC_REPORTERS: io.confluent.metrics.reporter.ConfluentMetricsReporter
            KAFKA_CONFLUENT_METRICS_REPORTER_BOOTSTRAP_SERVERS: kafka-broker3:9092
            KAFKA_CONFLUENT_METRICS_ENABLE: 'true'
            KAFKA_CONFLUENT_SUPPORT_CUSTOMER_ID: anonymous
            #### Self-Balancing Cluster (https://docs.confluent.io/platform/current/kafka/sbc/index.html) ####
            KAFKA_CONFLUENT_BALANCER_ENABLE: 'true'
            KAFKA_CONFLUENT_BALANCER_HEAL_UNEVEN_LOAD_TRIGGER: ANY_UNEVEN_LOAD
            KAFKA_CONFLUENT_BALANCER_HEAL_BROKER_FAILURE_THRESHOLD_MS: 900000
            KAFKA_CONFLUENT_REPORTERS_TELEMETRY_AUTO_ENABLE: 'true'

    kafka-broker4:
        image: ${CONTAINER_REGISTRY}confluentinc/cp-server:${CONFLUENT_VERSION}
        restart: always
        ports:
            - 49092:49092
        networks:
            default:
                aliases:
                    - kafka
        volumes:
            - kafka-broker4:/var/lib/kafka/data
        environment:
            #### Server Basics ####
            KAFKA_NODE_ID: 104 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_ZOOKEEPER_CONNECT: zookeeper1:2181,zookeeper2:2181,zookeeper3:2181 # (required) Instructs Kafka how to get in touch with ZooKeeper.
            #### Socket Server Settings ###
            KAFKA_ADVERTISED_LISTENERS: PLAINTEXT_HOST://localhost:49092,PLAINTEXT://kafka-broker4:9092 # (required) Describes how the host name that is advertised and can be reached by clients. PLAINTEXT_HOST://localhost:19092 enables access from Docker host.
            KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT_HOST:PLAINTEXT,PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT,SASL_SSL:SASL_SSL # (default: PLAINTEXT:PLAINTEXT,...) Map between listener names and security protocols. In this scenario this setting is used to define listeners with names.
            #### Defaults ####
            KAFKA_DEFAULT_REPLICATION_FACTOR: 3
            KAFKA_NUM_PARTITIONS: 12
            #### MD5 ####
            KAFKA_CONFLUENT_METADATA_SERVER_LISTENERS: http://0.0.0.0:8091
            KAFKA_CONFLUENT_METADATA_SERVER_ADVERTISED_LISTENERS: http://kafka-broker4:8091
            #### Metrics ####
            KAFKA_METRIC_REPORTERS: io.confluent.metrics.reporter.ConfluentMetricsReporter
            KAFKA_CONFLUENT_METRICS_REPORTER_BOOTSTRAP_SERVERS: kafka-broker4:9092
            KAFKA_CONFLUENT_METRICS_ENABLE: 'true'
            KAFKA_CONFLUENT_SUPPORT_CUSTOMER_ID: anonymous
            #### Self-Balancing Cluster (https://docs.confluent.io/platform/current/kafka/sbc/index.html) ####
            KAFKA_CONFLUENT_BALANCER_ENABLE: 'true'
            KAFKA_CONFLUENT_BALANCER_HEAL_UNEVEN_LOAD_TRIGGER: ANY_UNEVEN_LOAD
            KAFKA_CONFLUENT_BALANCER_HEAL_BROKER_FAILURE_THRESHOLD_MS: 900000
            # https://docs.confluent.io/platform/current/kafka/sbc/configuration_options.html#how-to-choose-a-throttle-setting
            KAFKA_CONFLUENT_BALANCER_THROTTLE_BYTES_PER_SECOND: 10485760
            KAFKA_CONFLUENT_REPORTERS_TELEMETRY_AUTO_ENABLE: 'true'

    #############################################################
    # Control Center                                            #
    #############################################################
    control-center:
        image: ${CONTAINER_REGISTRY}confluentinc/cp-enterprise-control-center:${CONFLUENT_VERSION}
        restart: always
        ports:
            - 9021:9021
        volumes:
            - control-center:/var/lib/confluent-control-center         
        environment:
            # General configuration
            CONTROL_CENTER_BOOTSTRAP_SERVERS: kafka:9092
            CONTROL_CENTER_ZOOKEEPER_CONNECT: zookeeper1:2181,zookeeper2:2181,zookeeper3:2181
            CONTROL_CENTER_REPLICATION_FACTOR: 3
            # Amount of heap to use for internal caches. Increase for better throughput
            CONTROL_CENTER_STREAMS_CACHE_MAX_BYTES_BUFFERING: 100000000
            CONTROL_CENTER_STREAMS_CONSUMER_REQUEST_TIMEOUT_MS: "960032"
            CONTROL_CENTER_STREAMS_NUM_STREAM_THREADS: 1
            # HTTP and HTTPS to Control Center UI 
            CONTROL_CENTER_REST_LISTENERS: http://0.0.0.0:9021
            # Used by Control Center to connect to the Admin API for Self Balancing Clusters
            CONTROL_CENTER_STREAMS_CPREST_URL: "http://kafka:8091"


    #############################################################
    # Kafka CLI                                                 #
    #############################################################
    cli-kafka:
        image: ${CONTAINER_REGISTRY}confluentinc/cp-server:${CONFLUENT_VERSION}
        hostname: cli-kafka
        entrypoint: /bin/bash
        tty: true

    cli-kcat:
        image: ${CONTAINER_REGISTRY}confluentinc/cp-kcat:${CONFLUENT_VERSION}
        hostname: cli-kcat
        entrypoint: /bin/bash
        tty: true


volumes:
    zookeeper1_data:
    zookeeper1_datalog:
    zookeeper2_data:
    zookeeper2_datalog:
    zookeeper3_data:
    zookeeper3_datalog:
    kafka-broker1:
    kafka-broker2:
    kafka-broker3:
    kafka-broker4:
    control-center:
