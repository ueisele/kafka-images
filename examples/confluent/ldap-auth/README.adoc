= Confluent Server with Ldap Authentication and RBAC

Simplified variant of link:https://github.com/confluentinc/cp-demo[confluentinc/cp-demo], which soley focuses on the ldap authentication and authorization aspect. Compared to the demo, it also supports authentication of Kafka protocol via SASL/PLAIN with Ldap identities.

.Create certificate for MDS token signing
[source,bash]
----
./create-certs.sh
----

.Start services
[source,bash]
----
podman-compose up -d openldap
podman-compose up -d zookeeper1 zookeeper2 zookeeper3
podman-compose up -d kafka-broker1 kafka-broker2 kafka-broker3
podman-compose up -d cli-kafka
----

.Check Ldap
[source,bash]
----
podman-compose exec openldap bash
ldapsearch -LLL -x -H ldap://openldap:389 -b 'dc=confluentdemo,dc=io' -D "cn=admin,dc=confluentdemo,dc=io" -w 'admin'
----

.Query cluster metadata
[source,bash]
----
podman-compose exec cli-kafka bash
curl http://kafka-broker1:8090/v1/metadata/id
----

== RBAC

.Connect with superUser
[source,bash]
----
podman-compose exec cli-kafka bash
kafka-topics --command-config /tmp/conf/superUser.config --bootstrap-server kafka:9092 --list
----

.Connect with appSA
[source,bash]
----
podman-compose exec cli-kafka bash
kafka-topics --command-config /tmp/conf/appSA.config --bootstrap-server kafka:9092 --list
----

.Confluent Cli login
[source,bash]
----
podman-compose exec confluent-cli bash
confluent login --url $CONFLUENT_PLATFORM_MDS_URL
----

Login with user `superUser` and password `superUser`.

.Confluent Cli cluster describe
[source,bash]
----
podman-compose exec confluent-cli bash
confluent cluster describe --url $CONFLUENT_PLATFORM_MDS_URL
apk add jq
export CLUSTER_ID="$(confluent cluster describe --url $CONFLUENT_PLATFORM_MDS_URL -o json | jq -r .crn)"
----

.Confluent Cli RBAC list
[source,bash]
----
podman-compose exec confluent-cli bash
confluent iam rbac role list
confluent iam rbac role-binding list --kafka-cluster-id $CLUSTER_ID --principal User:appSA
----

.Confluent Cli RBAC grant topic permission to appSA
[source,bash]
----
podman-compose exec confluent-cli bash
confluent iam rbac role-binding create \
    --principal User:appSA \
    --role DeveloperManage \
    --resource Topic:app- \
    --prefix \
    --kafka-cluster-id $CLUSTER_ID

confluent iam rbac role-binding create \
    --principal User:appSA \
    --role DeveloperWrite \
    --resource Topic:app- \
    --prefix \
    --kafka-cluster-id $CLUSTER_ID

confluent iam rbac role-binding create \
    --principal User:appSA \
    --role DeveloperRead \
    --resource Topic:app- \
    --prefix \
    --kafka-cluster-id $CLUSTER_ID
confluent iam rbac role-binding create \
    --principal User:appSA \
    --role DeveloperRead \
    --resource Group:app- \
    --prefix \
    --kafka-cluster-id $CLUSTER_ID

confluent iam rbac role-binding list --kafka-cluster-id $CLUSTER_ID --principal User:appSA
----

.Connect with appSA and create topic, produce and consume messages
[source,bash]
----
podman-compose exec cli-kafka bash

kafka-topics --command-config /tmp/conf/appSA.config --bootstrap-server kafka:9092 \
    --create --topic app-topic-a --replication-factor 3 --partitions 3

kafka-topics --command-config /tmp/conf/appSA.config --bootstrap-server kafka:9092 --list

echo "test_message" | kafka-console-producer \
    --broker-list kafka:9092 \
    --topic app-topic-a \
    --producer.config /tmp/conf/appSA.config \
    --property parse.key=false

kafka-console-consumer \
    --bootstrap-server kafka:9092 \
    --topic app-topic-a \
    --group app-cg \
    --consumer.config /tmp/conf/appSA.config  \
    --from-beginning \
    --property parse.key=false \
    --max-messages 1
----

also see:

* link:https://docs.confluent.io/platform/current/security/rbac/rbac-cli-quickstart.html[RBAC Cli Quick Start]
* link:https://docs.confluent.io/platform/current/security/rbac/rbac-predefined-roles.html[RBAC predefined roles]

== Quotas

.Set produce and consume quotas for appSA user
[source,bash]
----
podman-compose exec cli-kafka bash
kafka-configs  --bootstrap-server kafka:9092 --command-config /tmp/conf/superUser.config \
  --alter --add-config 'producer_byte_rate=102400,consumer_byte_rate=204800' --entity-type users --entity-name appSA
----

*Hint:* The quota is shared by all clients of the appSA user. The quota is defined on a per-broker basis. All clients of this user can publish/fetch a maximum of X bytes/sec per broker before they are throttled.

.Run a producer performance test with the appSA user with unlimited throughput
[source,bash]
----
podman-compose exec cli-kafka bash
kafka-producer-perf-test --producer.config /tmp/conf/appSA.config --producer-props bootstrap.servers=kafka:9092 client.id=appSA.perftest2 acks=all buffer.memory=1024000 \
  --topic app-topic-a --recd-size 10240 --throughput -1 --num-records 3000
----

.The producer will not be able to publish more than 30kb/sec, because it is allowed to publish 10kb per broker and we have partitions on all 3 brokers.
----
...
150 records sent, 29.7 records/sec (0.29 MB/sec)
150 records sent, 29.6 records/sec (0.29 MB/sec)
3000 records sent, 30.625995 records/sec (0.30 MB/sec)
----

.If you start two performance test at the same time, the throughput of both tests will go down to 15kb/sec, because the quota is shared by all clients of this user.
----
75 records sent, 14.8 records/sec (0.14 MB/sec)
75 records sent, 14.8 records/sec (0.14 MB/sec)
----

.Run a consumer performance test with the appSA user (requires that at least 3000 messages are in the topic)
[source,bash]
----
podman-compose exec cli-kafka bash
kafka-consumer-perf-test --consumer.config /tmp/conf/appSA.config --bootstrap-server kafka:9092 \
  --group app-perftest --topic app-topic-a --messages 3000 --reporting-interval 5000 --timeout 120000
----

.The max throughput is ~600kb/sec, because the quota is 200kb and we have partitions with the same number of messages on each broker.
----
time, threadId, data.consumed.in.MB, MB.sec, data.consumed.in.nMsg, nMsg.sec, rebalance.time.ms, fetch.time.ms, fetch.MB.sec, fetch.nMsg.sec
2022-09-14 10:14:12:761, 0, 5.9277, 0.7735, 607, 79.2015, 3280, 4384, 1.3521, 138.4580
2022-09-14 10:14:17:873, 0, 8.8867, 0.5788, 910, 59.2723, 0, 5112, 0.5788, 59.2723
2022-09-14 10:14:22:999, 0, 11.8457, 0.5773, 1213, 59.1104, 0, 5126, 0.5773, 59.1104
----

also see:

* link:https://kafka.apache.org/documentation/#design_quotas[Quotas Concept]
* link:https://kafka.apache.org/documentation/#quotas[Setting Quotas]