#############################################################
# KRaft Cluster Dedicated Mode                              #
# 3x controller + 3x broker                                 #
#############################################################
# https://github.com/apache/kafka/blob/2.8.0/config/kraft/README.md
# In KRaft mode, only a small group of specially selected servers can act as controllers 
# (unlike the ZooKeeper-based mode, where any server can become the Controller). 
# The specially selected controller servers will participate in the metadata quorum. 
# Each controller server is either active, or a hot standby for the current active controller server.
#
# You will typically select 3 or 5 servers for this role, 
# depending on factors like cost and the number of concurrent failures your system should withstand without availability impact. 
# Just like with ZooKeeper, you must keep a majority of the controllers alive in order to maintain availability. 
# So if you have 3 controllers, you can tolerate 1 failure; with 5 controllers, you can tolerate 2 failures.
---
services:
    
    #############################################################
    # Kafka Controller                                          #
    #############################################################
    kafka-controller1:
        image: ${CONTAINER_REGISTRY}ueisele/apache-kafka-server:${KAFKA_VERSION}
        restart: always
        ports:
            - 19093:9093
            - 9461:9464
        volumes:
            - kafka-controller1:/opt/apache/kafka/data
        environment:
            #### Setup ####
            CLUSTER_ID: ${CLUSTER_ID} # In KRaft mode the cluster id must be generated manually. All nodes in a cluster must have the same id.
            AUTO_FORMAT_KAFKA_STORAGE_DIR: "true" # (default: false) In KRaft mode also the storage dir must be formatted manually before the node is started the first time. If set to true, the directory is formatted if it is not formatted until now. However, be careful with this setting, could lead to data loss.
            #### Server Basics ####
            KAFKA_PROCESS_ROLES: controller # (default: "") Defines in which mode the server runs and which roles it has. Valid values are 'broker', 'controller' 'broker,controller' and ''. If empty the server runs in ZooKeeper mode.
            KAFKA_NODE_ID: 1 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_CONTROLLER_QUORUM_VOTERS: 1@kafka-controller1:9093,2@kafka-controller2:9093,3@kafka-controller3:9093 # (required in KRaft mode) The connect string for the controller quorum
            #### Socket Server Settings ###
            KAFKA_LISTENERS: PLAINTEXT://:9093 # (by default derived from advertised listeners) List of URIs we will listen on and the listener names. In this case, Kafka listens in both ports on all interfaces.
            KAFKA_CONTROLLER_LISTENER_NAMES: PLAINTEXT # (required in KRaft mode) A comma-separated list of the names of the listeners used by the controller.

    kafka-controller2:
        image: ${CONTAINER_REGISTRY}ueisele/apache-kafka-server:${KAFKA_VERSION}
        restart: always
        ports:
            - 29093:9093
            - 9462:9464
        volumes:
            - kafka-controller2:/opt/apache/kafka/data
        environment:
            #### Setup ####
            CLUSTER_ID: ${CLUSTER_ID} # In KRaft mode the cluster id must be generated manually. All nodes in a cluster must have the same id.
            AUTO_FORMAT_KAFKA_STORAGE_DIR: "true" # (default: false) In KRaft mode also the storage dir must be formatted manually before the node is started the first time. If set to true, the directory is formatted if it is not formatted until now. However, be careful with this setting, could lead to data loss.
            #### Server Basics ####
            KAFKA_PROCESS_ROLES: controller # (default: "") Defines in which mode the server runs and which roles it has. Valid values are 'broker', 'controller' 'broker,controller' and ''. If empty the server runs in ZooKeeper mode.
            KAFKA_NODE_ID: 2 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_CONTROLLER_QUORUM_VOTERS: 1@kafka-controller1:9093,2@kafka-controller2:9093,3@kafka-controller3:9093 # (required in KRaft mode) The connect string for the controller quorum
            #### Socket Server Settings ###
            KAFKA_LISTENERS: PLAINTEXT://:9093 # (by default derived from advertised listeners) List of URIs we will listen on and the listener names. In this case, Kafka listens in both ports on all interfaces.
            KAFKA_CONTROLLER_LISTENER_NAMES: PLAINTEXT # (required in KRaft mode) A comma-separated list of the names of the listeners used by the controller.

    kafka-controller3:
        image: ${CONTAINER_REGISTRY}ueisele/apache-kafka-server:${KAFKA_VERSION}
        restart: always
        ports:
            - 39093:9093
            - 9463:9464
        volumes:
            - kafka-controller3:/opt/apache/kafka/data
        environment:
            #### Setup ####
            CLUSTER_ID: ${CLUSTER_ID} # In KRaft mode the cluster id must be generated manually. All nodes in a cluster must have the same id.
            AUTO_FORMAT_KAFKA_STORAGE_DIR: "true" # (default: false) In KRaft mode also the storage dir must be formatted manually before the node is started the first time. If set to true, the directory is formatted if it is not formatted until now. However, be careful with this setting, could lead to data loss.
            #### Server Basics ####
            KAFKA_PROCESS_ROLES: controller # (default: "") Defines in which mode the server runs and which roles it has. Valid values are 'broker', 'controller' 'broker,controller' and ''. If empty the server runs in ZooKeeper mode.
            KAFKA_NODE_ID: 3 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_CONTROLLER_QUORUM_VOTERS: 1@kafka-controller1:9093,2@kafka-controller2:9093,3@kafka-controller3:9093 # (required in KRaft mode) The connect string for the controller quorum
            #### Socket Server Settings ###
            KAFKA_LISTENERS: PLAINTEXT://:9093 # (by default derived from advertised listeners) List of URIs we will listen on and the listener names. In this case, Kafka listens in both ports on all interfaces.
            KAFKA_CONTROLLER_LISTENER_NAMES: PLAINTEXT # (required in KRaft mode) A comma-separated list of the names of the listeners used by the controller.

    #############################################################
    # Kafka Broker                                              #
    #############################################################
    kafka-broker1:
        image: ${CONTAINER_REGISTRY}ueisele/apache-kafka-server:${KAFKA_VERSION}
        restart: always
        ports:
            - 9092:19092
            - 19092:19092
            - 9464:9464
        networks:
            default:
                aliases:
                    - kafka
        volumes:
            - kafka-broker1:/opt/apache/kafka/data
        environment:
            #### Setup ####
            CLUSTER_ID: ${CLUSTER_ID} # In KRaft mode the cluster id must be generated manually. All nodes in a cluster must have the same id.
            AUTO_FORMAT_KAFKA_STORAGE_DIR: "true" # (default: false) In KRaft mode also the storage dir must be formatted manually before the node is started the first time. If set to true, the directory is formatted if it is not formatted until now. However, be careful with this setting, could lead to data loss.
            #### Server Basics ####
            KAFKA_PROCESS_ROLES: broker # (default: "") Defines in which mode the server runs and which roles it has. Valid values are 'broker', 'controller' 'broker,controller' and ''. If empty the server runs in ZooKeeper mode.
            KAFKA_NODE_ID: 101 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_CONTROLLER_QUORUM_VOTERS: 1@kafka-controller1:9093,2@kafka-controller2:9093,3@kafka-controller3:9093 # (required in KRaft mode) The connect string for the controller quorum
            #### Socket Server Settings ###
            KAFKA_ADVERTISED_LISTENERS: HOST://localhost:19092,PLAINTEXT://kafka-broker1:9092 # (required) Describes how the host name that is advertised and can be reached by clients. HOST://localhost:19092 enables access from Docker host.
            KAFKA_CONTROLLER_LISTENER_NAMES: CONTROLLER # (required in KRaft mode) A comma-separated list of the names of the listeners used by the controller.
            KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: HOST:PLAINTEXT,CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT,SASL_SSL:SASL_SSL # (default: PLAINTEXT:PLAINTEXT,...) Map between listener names and security protocols. In this scenario this setting is used to define listeners with names.

    kafka-broker2:
        image: ${CONTAINER_REGISTRY}ueisele/apache-kafka-server:${KAFKA_VERSION}
        restart: always
        ports:
            - 29092:29092
            - 9465:9464
        networks:
            default:
                aliases:
                    - kafka
        volumes:
            - kafka-broker2:/opt/apache/kafka/data
        environment:
            #### Setup ####
            CLUSTER_ID: ${CLUSTER_ID} # In KRaft mode the cluster id must be generated manually. All nodes in a cluster must have the same id.
            AUTO_FORMAT_KAFKA_STORAGE_DIR: "true" # (default: false) In KRaft mode also the storage dir must be formatted manually before the node is started the first time. If set to true, the directory is formatted if it is not formatted until now. However, be careful with this setting, could lead to data loss.
            #### Server Basics ####
            KAFKA_PROCESS_ROLES: broker # (default: "") Defines in which mode the server runs and which roles it has. Valid values are 'broker', 'controller' 'broker,controller' and ''. If empty the server runs in ZooKeeper mode.
            KAFKA_NODE_ID: 102 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_CONTROLLER_QUORUM_VOTERS: 1@kafka-controller1:9093,2@kafka-controller2:9093,3@kafka-controller3:9093 # (required in KRaft mode) The connect string for the controller quorum
            #### Socket Server Settings ###
            KAFKA_ADVERTISED_LISTENERS: HOST://localhost:29092,PLAINTEXT://kafka-broker2:9092 # (required) Describes how the host name that is advertised and can be reached by clients. HOST://localhost:19092 enables access from Docker host.
            KAFKA_CONTROLLER_LISTENER_NAMES: CONTROLLER # (required in KRaft mode) A comma-separated list of the names of the listeners used by the controller.
            KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: HOST:PLAINTEXT,CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT,SASL_SSL:SASL_SSL # (default: PLAINTEXT:PLAINTEXT,...) Map between listener names and security protocols. In this scenario this setting is used to define listeners with names.

    kafka-broker3:
        image: ${CONTAINER_REGISTRY}ueisele/apache-kafka-server:${KAFKA_VERSION}
        restart: always
        ports:
            - 39092:39092
            - 9466:9464
        networks:
            default:
                aliases:
                    - kafka
        volumes:
            - kafka-broker3:/opt/apache/kafka/data
        environment:
            #### Setup ####
            CLUSTER_ID: ${CLUSTER_ID} # In KRaft mode the cluster id must be generated manually. All nodes in a cluster must have the same id.
            AUTO_FORMAT_KAFKA_STORAGE_DIR: "true" # (default: false) In KRaft mode also the storage dir must be formatted manually before the node is started the first time. If set to true, the directory is formatted if it is not formatted until now. However, be careful with this setting, could lead to data loss.
            #### Server Basics ####
            KAFKA_PROCESS_ROLES: broker # (default: "") Defines in which mode the server runs and which roles it has. Valid values are 'broker', 'controller' 'broker,controller' and ''. If empty the server runs in ZooKeeper mode.
            KAFKA_NODE_ID: 103 # (default: -1, required in KRaft mode) The node id for this server.
            KAFKA_CONTROLLER_QUORUM_VOTERS: 1@kafka-controller1:9093,2@kafka-controller2:9093,3@kafka-controller3:9093 # (required in KRaft mode) The connect string for the controller quorum
            #### Socket Server Settings ###
            KAFKA_ADVERTISED_LISTENERS: HOST://localhost:39092,PLAINTEXT://kafka-broker3:9092 # (required) Describes how the host name that is advertised and can be reached by clients. HOST://localhost:19092 enables access from Docker host.
            KAFKA_CONTROLLER_LISTENER_NAMES: CONTROLLER # (required in KRaft mode) A comma-separated list of the names of the listeners used by the controller.
            KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: HOST:PLAINTEXT,CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT,SASL_SSL:SASL_SSL # (default: PLAINTEXT:PLAINTEXT,...) Map between listener names and security protocols. In this scenario this setting is used to define listeners with names.

volumes:
    kafka-controller1:
    kafka-controller2:
    kafka-controller3:
    kafka-broker1:
    kafka-broker2:
    kafka-broker3:
