#!/usr/bin/env bash
set -e
EXAMPLE_DIR="$(readlink -f "$(dirname ${BASH_SOURCE[0]})")"

source "${EXAMPLE_DIR}/.env"
source "${EXAMPLE_DIR}/generate-ca"
source "${EXAMPLE_DIR}/generate-cert"
source "${EXAMPLE_DIR}/btpl"

export TARGET_DIR="${EXAMPLE_DIR}/target"
export CA_KEY_PASS="cakeypass"
export CERT_KEY_PASS="certkeypass"

###########################################
# Cluster CA                              #
###########################################

export GROUP=cluster

CA_DNAME="/C=de/O=examples/O=kraft/O=cluster-mtls-all-pem/CN=cluster-ca" \
generateCA

###########################################
# Server                                  #
###########################################

NAME=server \
CERT_DNAME="/C=de/O=examples/O=kraft/O=cluster-mtls-all-pem/OU=cluster/CN=kafka-server" \
CERT_SAN="IP:127.0.0.1,DNS:localhost,DNS:kafka,DNS:kafka-broker1,DNS:kafka-broker2,DNS:kafka-broker3" \
generateCert

###########################################
# Controller                              #
###########################################

NAME=controller \
CERT_DNAME="/C=de/O=examples/O=kraft/O=cluster-mtls-all-pem/OU=cluster/CN=kafka-controller" \
CERT_SAN="IP:127.0.0.1,DNS:localhost,DNS:kafka-controller1,DNS:kafka-controller2,DNS:kafka-controller3" \
generateCert


###########################################
# Clients CA                              #
###########################################

export GROUP=clients

CA_DNAME="/C=de/O=examples/O=kraft/O=cluster-mtls-all-pem/CN=clients-ca" \
generateCA

###########################################
# Users                                   #
###########################################

export KAFKA_USER="kafka"

NAME="${KAFKA_USER}" \
CERT_DNAME="/C=de/O=examples/O=kraft/O=cluster-mtls-all-pem/OU=clients/OU=admins/CN=${KAFKA_USER}" \
generateCert

btpl "${EXAMPLE_DIR}/clients.key-enc-pbes2.properties.btpl" "${TARGET_DIR}/clients.${KAFKA_USER}.key-enc-pbes2.properties"
btpl "${EXAMPLE_DIR}/clients.key-enc-default.properties.btpl" "${TARGET_DIR}/clients.${KAFKA_USER}.key-enc-default.properties"
btpl "${EXAMPLE_DIR}/clients.key-noenc.properties.btpl" "${TARGET_DIR}/clients.${KAFKA_USER}.key-noenc.properties"

###########################################
# Post Processing                         #
###########################################

chmod -R u=rwX,go=rX "${TARGET_DIR}"
