= Example: Connect Standalone - HTTP Source (Avro)

.Ensure that 'mock-server.initializer.json' is readable by anyone.
[source,bash]
----
chmod o=r mock-server.initializer.json
----

.Run with podman-compose
[source,bash]
----
podman-compose up
----