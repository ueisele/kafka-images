# Standalone Container Image for Apache Kafka Broker and Controller

Container image for running the [Open Source version of Apache Kafka](https://github.com/apache/kafka/).
It offers support for running a single Apache Kafka instance in Kafka [KRaft mode](https://github.com/apache/kafka/blob/3.3.1/config/kraft/README.md).

The Kafka distribution included in the Container image is built directly from [source](https://github.com/apache/kafka/).

The standalone Container image is based on [ueisele/apache-kafka-server](https://hub.docker.com/repository/docker/ueisele/apache-kafka-server). 

The Container images are available on Docker Hub repository [ueisele/apache-kafka-server-standalone](https://hub.docker.com/repository/docker/ueisele/apache-kafka-server), and the source files for the images are available on GitLab repository [ueisele/kafka-images](https://gitlab.com/ueisele/kafka-images).

## Most Recent Tags

Most recent tags for `RELEASE` builds:

* `3.8.1`, `3.8.1-zulu21`, `3.8.1-zulu21.0.5`, `3.8.1-zulu21-alma9.5`, `3.8.1-zulu21.0.5-alma9.5-20241118`
* `3.9.0`, `3.9.0-zulu21`, `3.9.0-zulu21.0.5`, `3.9.0-zulu21-alma9.5`, `3.9.0-zulu21.0.5-alma9.5-20241118`

Most recent tags for `SNAPSHOT` builds:

* `4.0.0-SNAPSHOT`, `4.0.0-SNAPSHOT-zulu21`, `4.0.0-SNAPSHOT-zulu21.0.5`, `4.0.0-SNAPSHOT-zulu21-alma9.5`, `4.0.0-SNAPSHOT-zulu21.0.5-alma9.5-20241118`

Additionally, a tag with the associated Git-Sha of the built Apache Kafka distribution is always published as well, e.g. `ueisele/apache-kafka-server-standalone:4.0.0-SNAPSHOT-gfd9de50-zulu21.0.5-alma9.5-20241118`.

## Image

The Container images are based on [ueisele/zulu-openjdk-micro](https://hub.docker.com/repository/docker/ueisele/zulu-openjdk-micro) with _JRE_ installed (e.g. _21-jre_). 

The OpenJDK image in turn is based on [AlmaLinux 9 Micro](https://hub.docker.com/r/almalinux/9-micro).

As OpenJDK [Azul Zulu](https://www.azul.com/downloads/?package=jdk) is used.
Azul Zulu builds of OpenJDK are fully tested and TCK compliant builds of OpenJDK.

## Quick Start

To start a single Kafka server instance in KRaft mode just run: 

```bash
podman run --rm -p 19092:19092 -p 9464:9464 ueisele/apache-kafka-server-standalone:3.9.0
```

If you use Podman, the container network is not reachable from localhost.
You can use port 19092, which exposes `localhost:19092` as advertised listener. 

If you use Docker, you can directly use port 9092 and bind it to any host port.
The reason is, that `<container-ip>:9092` is exposed, which is reachable if Docker is used.

```bash
docker run --rm -p 9092:9092 -p 9464:9464 ueisele/apache-kafka-server-standalone:3.9.0
```

To start a single Kafka instance in KRaft mode with Ipv6 just run: 

```bash
podman network create --ipv6 --subnet fd01::/80 kafka-standalone
podman run --rm -p 19092:19092 -p 9464:9464 --net kafka-standalone -e STANDALONE_BROKER_IP_VERSION=ipv6 ueisele/apache-kafka-server-standalone:3.9.0
```

## Configuration

The [ueisele/apache-kafka-server-standalone](https://hub.docker.com/repository/registry-1.docker.io/ueisele/apache-kafka-server/) image just sets environment variables which are required for a standalone execution (see [Containerfile](server-standalone/Containerfile)). 

The configuration is identical to the [ueisele/apache-kafka-server](https://hub.docker.com/repository/docker/ueisele/apache-kafka-server) image and therefore also fully compatible with the [Confluent Docker images](https://docs.confluent.io/platform/current/installation/docker/config-reference.html#confluent-ak-configuration).

For the Apache Kafka ([ueisele/apache-kafka-server-standalone](https://hub.docker.com/repository/registry-1.docker.io/ueisele/apache-kafka-server-standalone/)) image, convert the [Apache Kafka broker configuration properties](https://kafka.apache.org/documentation/#brokerconfigs) as below and use them as environment variables:

* Prefix with KAFKA_.
* Convert to upper-case.
* Replace a period (.) with a single underscore (_).
* Replace a dash (-) with double underscores (__).
* Replace an underscore (_) with triple underscores (___).

### Listeners

By default the standalone Kafka opens a listener on port 9092 on all interfaces and advertises the first found Ip address.

However, you can overwrite the advertised listener and the listener of the broker with `STANDALONE_BROKER_LISTENERS` and `STANDALONE_BROKER_ADVERTISED_LISTENERS`.
For example `STANDALONE_BROKER_LISTENERS=PLAINTEXT://127.0.0.1:9092` and `STANDALONE_BROKER_ADVERTISED_LISTENERS=PLAINTEXT://localhost:9092`.

The security protocol map for the broker can be set with `STANDALONE_BROKER_LISTENER_SECURITY_PROTOCOL_MAP`.

If you only want to change the port of the broker, you can set `STANDALONE_BROKER_PORT`.

In addition to the `STANDALONE_BROKER_` variables, you can also directly specify `KAFKA_` variables.
`STANDALONE_BROKER_` variables are just a convenient way to configure the standalone Kafka instance without the need to also set the controller configuration. 

### Logging

The logging configuration can be adjusted with the following environment variables:

* `KAFKA_LOG4J_PATTERN` sets the logging pattern (default: `[%d] (%t) %p %m (%c)%n`)
* `KAFKA_LOG4J_ROOT_LOGLEVEL` sets the root log level (default: `INFO`)
* `KAFKA_LOG4J_LOGGERS` is a comma separated list of logger and log level key-value pairs 
  (default: `kafka=INFO,kafka.network.RequestChannel$=WARN,kafka.producer.async.DefaultEventHandler=DEBUG,kafka.request.logger=WARN,kafka.controller=TRACE,kafka.log.LogCleaner=INFO,state.change.logger=TRACE,kafka.authorizer.logger=WARN`)

### JMX

Remote JMX can be enabled with the following environment variables:

```properties
KAFKA_JMX_PORT=6001
KAFKA_JMX_HOSTNAME=localhost
```

### Prometheus

The image contains the [Prometheus JMX Exporter JavaAgent](https://github.com/prometheus/jmx_exporter). In the default configuration, this exports metrics via a [Prometheus](https://prometheus.io/) endpoint on port _9464_.
As configuration for the Kafka metrics, the file [default.yaml](https://gitlab.com/ueisele/kafka-images/-/blob/main/server/include/opt/apache/kafka/javaagents/prometheus/configs/default.yaml) is used. 

The Prometheus JMX Exporter JavaAgent can be configured via environment variables:

> #### `PROMETHEUS_JAVAAGENT_ENABLED`
> Can be used to disable the Prometheus JMX Exporter JavaAgent.
> *   Type: `Boolean`
> *   Default: `true`
>
> #### `PROMETHEUS_EXPORTER_CONFIG`
> To provide your own metric definitions, create a [YAML configuration file](https://github.com/prometheus/jmx_exporter), and specify their location.
> The provided [default.yaml](https://gitlab.com/ueisele/kafka-images/-/blob/main/server/include/opt/apache/kafka/javaagents/prometheus/configs/default.yaml) can be used as an example.
> *   Type: `String`
> *   Default: `/opt/apache/kafka/javaagents/prometheus/configs/default.yaml`
>
> #### `PROMETHEUS_EXPORTER_PORT`
> The port on which the prometheus metrics are provided.
> *   Type: `Integer`
> *   Default: `9464`
>

### Debugging

In order to debug Apache Kafka, set the following environment variable:

```properties
KAFKA_DEBUG=y
```

In addition you can configure the behavior with the following environment variables:

```properties
JAVA_DEBUG_PORT=5005
DEBUG_SUSPEND_FLAG=y
```

## Build

In order to create your own Container image for Apache Kafka clone the [ueisele/kafka-image](https://gitlab.com/ueisele/kafka-images) Git repository and run the build command:

```bash
git clone https://gitlab.com/ueisele/kafka-images.git
cd kafka-images
server-standalone/build.sh --build --tag 3.9.0 --openjdk-release 21
```

To create an image with a specific OpenJDK version use the following command:

```bash
server-standalone/build.sh --build --tag 3.9.0 --openjdk-release 21 --openjdk-version 21.0.5
```

To build the most recent `SNAPSHOT` of Apache Kafka 3.9.0 with Java 21, run:

```bash
server-standalone/build.sh --build --branch trunk --openjdk-release 21
```

### Build Options

The `server-standalone/build.sh` script provides the following options:

`Usage: server-standalone/build.sh [--build] [--push] [--registry docker.io] [--user ueisele] [--archs amd64,arm64] [--github-repo apache/kafka] [--commit-sha 60e8456] [--tag 3.9.0] [--branch trunk] [--pull-request 9999] [--openjdk-release 21] [--openjdk-version 21.0.5]`

## License 

This Container image is licensed under the [Apache 2 license](https://gitlab.com/ueisele/kafka-images/-/blob/main/LICENSE).