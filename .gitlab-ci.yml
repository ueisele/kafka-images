##############################################
# Gitlab CI Pipeline Kafka Images            #
##############################################
# Variables for pipelines triggered by 'schedule' or 'web':
#   PIPELINE_RUN_ALWAYS=true
#   PIPELINE_REF_TYPE=branch|tag
#
#   PIPELINE_KCAT_REF=1.7.1|master
#   PIPELINE_OPENJDK_RELEASE=17|21
#   PIPELINE_KAFKA_REF=3.7.1|3.9.0|trunk
#
#   PIPELINE_DISABLED_KCAT=true
#   PIPELINE_DISABLED_OPENJDK=true
#   PIPELINE_DISABLED_KAFKA_SERVER=true
#   PIPELINE_DISABLED_KAFKA_CONNECT=true

# Bug in buildah 1.34.0 (https://github.com/containers/buildah/issues/5332)
image: quay.io/buildah/stable:v1.33.2

stages:
  - base
  - source
  - assembly
  - variant

variables:
  ARCHS: amd64,arm64

.buildah-login:
  before_script:
    - buildah login docker.io -u "${DOCKERHUB_USERNAME}" -p "${DOCKERHUB_TOKEN}"


##############################################
# KCat                                       #
##############################################

kcat:
  stage: source
  before_script:
    - !reference [.buildah-login, before_script]
    - dnf install -y git
  script:
    - ./kcat/build.sh --build --${REF_TYPE} ${KCAT_REF} --librdkafka-version ${LIBRDKAFKA_VERSION} --archs ${ARCHS}
    - ./kcat/build.sh --push  --${REF_TYPE} ${KCAT_REF} --librdkafka-version ${LIBRDKAFKA_VERSION} --archs ${ARCHS}
  parallel:
    matrix:
      - KCAT_REF: ["1.7.1"]
        REF_TYPE: tag
        LIBRDKAFKA_VERSION: ["2.6.1"]
      - KCAT_REF: ["master"]
        REF_TYPE: branch
        LIBRDKAFKA_VERSION: ["2.6.1"]        
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - kcat/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - kcat/**/*
      when: manual
    - if: | 
          $CI_PIPELINE_SOURCE == 'schedule' && $PIPELINE_DISABLED_KCAT != 'true' 
            && ($PIPELINE_KCAT_REF == $KCAT_REF || $PIPELINE_KCAT_REF == null) && ($PIPELINE_REF_TYPE == $REF_TYPE || $PIPELINE_REF_TYPE == null)
    - if: | 
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS == 'true' && $PIPELINE_DISABLED_KCAT != 'true' 
            && ($PIPELINE_KCAT_REF == $KCAT_REF || $PIPELINE_KCAT_REF == null) && ($PIPELINE_REF_TYPE == $REF_TYPE || $PIPELINE_REF_TYPE == null)
    - if: |
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS != 'true' && $PIPELINE_DISABLED_KCAT != 'true' 
            && ($PIPELINE_KCAT_REF == $KCAT_REF || $PIPELINE_KCAT_REF == null) && ($PIPELINE_REF_TYPE == $REF_TYPE || $PIPELINE_REF_TYPE == null)
      when: manual


##############################################
# OpenJDK                                    #
##############################################

.openjdk:
  stage: base
  before_script:
    - !reference [.buildah-login, before_script]
  script:
    - ./openjdk/${KIND}/build.sh --build --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}
    - ./openjdk/${KIND}/build.sh --push  --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}  
  parallel:
    matrix:
      - OPENJDK_RELEASE: [23,21,17]
  needs: []

.openjdk-rules:
  rules:
    - if: |
          $CI_PIPELINE_SOURCE == 'schedule' && $PIPELINE_DISABLED_OPENJDK != 'true' 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: |
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS == 'true' && $PIPELINE_DISABLED_OPENJDK != 'true' 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: |
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS != 'true' && $PIPELINE_DISABLED_OPENJDK != 'true' 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
      when: manual

openjdk-minimal:
  extends: .openjdk
  variables:
    KIND: minimal
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/minimal/**/*
        - openjdk/env
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/minimal/**/*
        - openjdk/env
      when: manual
    - !reference [.openjdk-rules, rules]

openjdk-micro:
  extends: .openjdk
  variables:
    KIND: micro
  before_script:
    - sudo -E -u build buildah login docker.io -u "${DOCKERHUB_USERNAME}" -p "${DOCKERHUB_TOKEN}"
  script:
    # must be executed as user 'build' in image 'quay.io/buildah/stable:latest'
    - sudo -E -u build buildah unshare bash -c "./openjdk/${KIND}/build.sh --build --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}"
    - sudo -E -u build ./openjdk/${KIND}/build.sh --push --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
      when: manual
    - !reference [.openjdk-rules, rules]


##############################################
# Kafka                                      #
##############################################

.kafka:
  before_script:
    - !reference [.buildah-login, before_script]
    - dnf install -y git
  script:
    - ./${COMPONENT}/build.sh --build --${REF_TYPE} ${KAFKA_REF} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS} ${COMPONENT_BUILD_OPTS}
    - ./${COMPONENT}/build.sh --push  --${REF_TYPE} ${KAFKA_REF} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}
  parallel:
    matrix:
      - KAFKA_REF: "3.8.1"
        REF_TYPE: tag
        OPENJDK_RELEASE: 21
      - KAFKA_REF: "3.9.0"
        REF_TYPE: tag
        OPENJDK_RELEASE: 21                
      - KAFKA_REF: "trunk"
        REF_TYPE: branch
        OPENJDK_RELEASE: 21


### Kafka Server #############################

.kafka-server-rules:
  rules:
    - if: | 
          $CI_PIPELINE_SOURCE == 'schedule' && $PIPELINE_DISABLED_KAFKA_SERVER != 'true' 
            && ($PIPELINE_KAFKA_REF == $KAFKA_REF || $PIPELINE_KAFKA_REF == null) && ($PIPELINE_REF_TYPE == $REF_TYPE || $PIPELINE_REF_TYPE == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: | 
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS == 'true' && $PIPELINE_DISABLED_KAFKA_SERVER != 'true' 
            && ($PIPELINE_KAFKA_REF == $KAFKA_REF || $PIPELINE_KAFKA_REF == null) && ($PIPELINE_REF_TYPE == $REF_TYPE || $PIPELINE_REF_TYPE == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: |
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS != 'true' && $PIPELINE_DISABLED_KAFKA_SERVER != 'true' 
            && ($PIPELINE_KAFKA_REF == $KAFKA_REF || $PIPELINE_KAFKA_REF == null) && ($PIPELINE_REF_TYPE == $REF_TYPE || $PIPELINE_REF_TYPE == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
      when: manual

kafka-server:
  stage: source
  extends: .kafka
  variables:
    COMPONENT: server
    COMPONENT_BUILD_OPTS: --patch --cache-maven ${CI_PROJECT_DIR}/.m2/ --cache-gradle ${CI_PROJECT_DIR}/.gradle/
  cache:
    key: ${CI_COMMIT_REF_SLUG}-${CI_JOB_NAME_SLUG}
    fallback_keys:
      - ${CI_COMMIT_REF_SLUG}
    paths:
      - .m2/
      - .gradle/
  needs: 
    - job: openjdk-minimal
      optional: true   
    - job: openjdk-micro
      optional: true          
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - server/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - server/**/*
      when: manual
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $REF_TYPE == 'branch'
      changes: 
        - server-otel/**/*
        - server-standalone/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $REF_TYPE == 'branch'
      changes: 
        - server-otel/**/*
        - server-standalone/**/*    
      when: manual      
    - !reference [.kafka-server-rules, rules]

kafka-server-standalone:
  stage: variant
  extends: .kafka
  variables:
    COMPONENT: server-standalone
  needs: 
    - job: kafka-server
      optional: true
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - server/**/*
        - server-standalone/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - server/**/*
        - server-standalone/**/*
      when: manual
    - !reference [.kafka-server-rules, rules]


### Kafka Connect ############################

kafka-connect-plugin-install:
  stage: base
  before_script:
    - sudo -E -u build buildah login docker.io -u "${DOCKERHUB_USERNAME}" -p "${DOCKERHUB_TOKEN}"
  script:
    # must be executed as user 'build' in image 'quay.io/buildah/stable:latest'
    - sudo -E -u build buildah unshare bash -c "./connect-plugin-install/build.sh --build --archs ${ARCHS}"
    - sudo -E -u build ./connect-plugin-install/build.sh --push  --archs ${ARCHS}      
  needs: 
    - job: openjdk-micro
      optional: true   
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-plugin-install/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-plugin-install/**/*
      when: manual 
    - if: $CI_PIPELINE_SOURCE == 'schedule' && $PIPELINE_DISABLED_KAFKA_CONNECT != 'true' 
    - if: $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS == 'true' && $PIPELINE_DISABLED_KAFKA_CONNECT != 'true' 
    - if: $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS != 'true' && $PIPELINE_DISABLED_KAFKA_CONNECT != 'true' 
      when: manual

.kafka-connect-rules:
  rules:
    - if: | 
          $CI_PIPELINE_SOURCE == 'schedule' && $PIPELINE_DISABLED_KAFKA_CONNECT != 'true' 
            && ($PIPELINE_KAFKA_REF == $KAFKA_REF || $PIPELINE_KAFKA_REF == null) && ($PIPELINE_REF_TYPE == $REF_TYPE || $PIPELINE_REF_TYPE == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: | 
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS == 'true' && $PIPELINE_DISABLED_KAFKA_CONNECT != 'true' 
            && ($PIPELINE_KAFKA_REF == $KAFKA_REF || $PIPELINE_KAFKA_REF == null) && ($PIPELINE_REF_TYPE == $REF_TYPE || $PIPELINE_REF_TYPE == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: |
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS != 'true' && $PIPELINE_DISABLED_KAFKA_CONNECT != 'true' 
            && ($PIPELINE_KAFKA_REF == $KAFKA_REF || $PIPELINE_KAFKA_REF == null) && ($PIPELINE_REF_TYPE == $REF_TYPE || $PIPELINE_REF_TYPE == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
      when: manual

kafka-connect-base:
  stage: source
  extends: .kafka
  variables:
    COMPONENT: connect-base
    COMPONENT_BUILD_OPTS: --patch --cache-maven ${CI_PROJECT_DIR}/.m2/ --cache-gradle ${CI_PROJECT_DIR}/.gradle/
  cache:
    key: ${CI_COMMIT_REF_SLUG}-${CI_JOB_NAME_SLUG}
    fallback_keys:
      - ${CI_COMMIT_REF_SLUG}
    paths:
      - .m2/
      - .gradle/    
  needs: 
    - job: openjdk-minimal
      optional: true   
    - job: openjdk-micro
      optional: true       
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-base/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-base/**/*
      when: manual
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $REF_TYPE == 'branch'
      changes: 
        - connect/**/*
        - connect-dev/**/*
        - connect-otel/**/*
        - connect-standalone/**/*
        - connect-standalone-dev/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $REF_TYPE == 'branch'
      changes: 
        - connect/**/*
        - connect-dev/**/*
        - connect-otel/**/*
        - connect-standalone/**/*
        - connect-standalone-dev/**/*
      when: manual      
    - !reference [.kafka-connect-rules, rules]

kafka-connect:
  stage: variant
  extends: .kafka
  variables:
    COMPONENT: connect
  needs: 
    - job: kafka-connect-plugin-install
      optional: true    
    - job: kafka-connect-base
      optional: true    
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-base/**/*
        - connect/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-base/**/*
        - connect/**/*
      when: manual
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $REF_TYPE == 'branch'
      changes: 
        - connect-otel/**/*
        - connect-dev/**/*
        - connect-standalone/**/*
        - connect-standalone-dev/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $REF_TYPE == 'branch'
      changes: 
        - connect-otel/**/*
        - connect-dev/**/*
        - connect-standalone/**/*
        - connect-standalone-dev/**/*
      when: manual      
    - !reference [.kafka-connect-rules, rules]

kafka-connect-dev:
  stage: variant
  extends: .kafka
  variables:
    COMPONENT: connect-dev
  before_script:
    - sudo -E -u build buildah login docker.io -u "${DOCKERHUB_USERNAME}" -p "${DOCKERHUB_TOKEN}"
    - dnf install -y git
  script:
    # must be executed as user 'build' in image 'quay.io/buildah/stable:latest'
    - sudo -E -u build buildah unshare bash -c "./${COMPONENT}/build.sh --build --${REF_TYPE} ${KAFKA_REF} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}"
    - sudo -E -u build ./${COMPONENT}/build.sh --push  --${REF_TYPE} ${KAFKA_REF} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}
  needs: 
    - job: kafka-connect-plugin-install
      optional: true    
    - job: kafka-connect
      optional: true    
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-plugin-install/**/*
        - connect-base/**/*
        - connect/**/*
        - connect-dev/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-plugin-install/**/*
        - connect-base/**/*
        - connect/**/*
        - connect-dev/**/*
      when: manual
    - !reference [.kafka-connect-rules, rules]

kafka-connect-standalone:
  stage: variant
  extends: .kafka
  variables:
    COMPONENT: connect-standalone
  needs: 
    - job: kafka-connect
      optional: true    
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-base/**/*
        - connect/**/*
        - connect-standalone/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-base/**/*
        - connect/**/*
        - connect-standalone/**/*
      when: manual   
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $REF_TYPE == 'branch'
      changes: 
        - connect-standalone-dev/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $REF_TYPE == 'branch'
      changes: 
        - connect-standalone-dev/**/*
      when: manual            
    - !reference [.kafka-connect-rules, rules]    

kafka-connect-standalone-dev:
  stage: variant
  extends: .kafka
  variables:
    COMPONENT: connect-standalone-dev
  before_script:
    - sudo -E -u build buildah login docker.io -u "${DOCKERHUB_USERNAME}" -p "${DOCKERHUB_TOKEN}"
    - dnf install -y git
  script:
    # must be executed as user 'build' in image 'quay.io/buildah/stable:latest'
    - sudo -E -u build buildah unshare bash -c "./${COMPONENT}/build.sh --build --${REF_TYPE} ${KAFKA_REF} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}"
    - sudo -E -u build ./${COMPONENT}/build.sh --push  --${REF_TYPE} ${KAFKA_REF} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}    
  needs: 
    - job: kafka-connect-plugin-install
      optional: true      
    - job: kafka-connect-standalone
      optional: true    
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-plugin-install/**/*
        - connect-base/**/*
        - connect/**/*
        - connect-standalone/**/*
        - connect-standalone-dev/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - connect-plugin-install/**/*
        - connect-base/**/*
        - connect/**/*
        - connect-standalone/**/*
        - connect-standalone-dev/**/*
      when: manual         
    - !reference [.kafka-connect-rules, rules]


##############################################
# Confluent Schema Registry                  #
##############################################

cp-schema-registry:
  stage: assembly
  before_script:
    - !reference [.buildah-login, before_script]
  script:
    - ./cp-schema-registry/build.sh --build --version ${CP_SCHEMA_REGISTRY_VERSION} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS} --cache-maven ${CI_PROJECT_DIR}/.m2/
    - ./cp-schema-registry/build.sh --push  --version ${CP_SCHEMA_REGISTRY_VERSION} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}
  cache:
    key: ${CI_COMMIT_REF_SLUG}-${CI_JOB_NAME_SLUG}
    fallback_keys:
      - ${CI_COMMIT_REF_SLUG}
    paths:
      - .m2/
  parallel:
    matrix:
      - CP_SCHEMA_REGISTRY_VERSION: 7.8.0
        OPENJDK_RELEASE: 21        
  needs: 
    - job: openjdk-minimal
      optional: true   
    - job: openjdk-micro
      optional: true          
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - cp-schema-registry/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - cp-schema-registry/**/*
      when: manual  
    - if: | 
          $CI_PIPELINE_SOURCE == 'schedule' && $PIPELINE_DISABLED_CP_SCHEMA_REGISTRY != 'true' 
            && ($PIPELINE_CP_SCHEMA_REGISTRY_VERSION == $CP_SCHEMA_REGISTRY_VERSION || $PIPELINE_CP_SCHEMA_REGISTRY_VERSION == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: | 
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS == 'true' && $PIPELINE_DISABLED_CP_SCHEMA_REGISTRY != 'true' 
            && ($PIPELINE_CP_SCHEMA_REGISTRY_VERSION == $CP_SCHEMA_REGISTRY_VERSION || $PIPELINE_CP_SCHEMA_REGISTRY_VERSION == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: |
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS != 'true' && $PIPELINE_DISABLED_CP_SCHEMA_REGISTRY != 'true' 
            && ($PIPELINE_CP_SCHEMA_REGISTRY_VERSION == $CP_SCHEMA_REGISTRY_VERSION || $PIPELINE_CP_SCHEMA_REGISTRY_VERSION == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
      when: manual

##############################################
# Confluent Schema Registry Tools            #
##############################################

cp-schema-registry-tools:
  stage: assembly
  before_script:
    - !reference [.buildah-login, before_script]
  script:
    - ./cp-schema-registry-tools/build.sh --build --version ${CP_SCHEMA_REGISTRY_VERSION} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS} --cache-maven ${CI_PROJECT_DIR}/.m2/
    - ./cp-schema-registry-tools/build.sh --push  --version ${CP_SCHEMA_REGISTRY_VERSION} --openjdk-release ${OPENJDK_RELEASE} --archs ${ARCHS}
  cache:
    key: ${CI_COMMIT_REF_SLUG}-${CI_JOB_NAME_SLUG}
    fallback_keys:
      - ${CI_COMMIT_REF_SLUG}
    paths:
      - .m2/
  parallel:
    matrix:
      - CP_SCHEMA_REGISTRY_VERSION: 7.8.0
        OPENJDK_RELEASE: 21        
  needs: 
    - job: openjdk-minimal
      optional: true   
    - job: openjdk-micro
      optional: true          
  rules:
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - cp-schema-registry-tools/**/*
    - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: 
        - openjdk/micro/**/*
        - openjdk/env
        - cp-schema-registry-tools/**/*
      when: manual  
    - if: | 
          $CI_PIPELINE_SOURCE == 'schedule' && $PIPELINE_DISABLED_CP_SCHEMA_REGISTRY != 'true' 
            && ($PIPELINE_CP_SCHEMA_REGISTRY_VERSION == $CP_SCHEMA_REGISTRY_VERSION || $PIPELINE_CP_SCHEMA_REGISTRY_VERSION == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: | 
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS == 'true' && $PIPELINE_DISABLED_CP_SCHEMA_REGISTRY != 'true' 
            && ($PIPELINE_CP_SCHEMA_REGISTRY_VERSION == $CP_SCHEMA_REGISTRY_VERSION || $PIPELINE_CP_SCHEMA_REGISTRY_VERSION == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
    - if: |
          $CI_PIPELINE_SOURCE == 'web' && $PIPELINE_RUN_ALWAYS != 'true' && $PIPELINE_DISABLED_CP_SCHEMA_REGISTRY != 'true' 
            && ($PIPELINE_CP_SCHEMA_REGISTRY_VERSION == $CP_SCHEMA_REGISTRY_VERSION || $PIPELINE_CP_SCHEMA_REGISTRY_VERSION == null) 
            && ($PIPELINE_OPENJDK_RELEASE == $OPENJDK_RELEASE || $PIPELINE_OPENJDK_RELEASE == null)
      when: manual
