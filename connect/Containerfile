ARG BUILDPLATFORM
ARG TARGETPLATFORM
ARG KAFKA_CONNECT_PLUGIN_INSTALL_IMAGE
ARG OPENJDK_JDK_IMAGE
ARG KAFKA_CONNECT_BASE_IMAGE
#################################
# Install Connect Plugins       #
#################################
FROM --platform=$BUILDPLATFORM ${KAFKA_CONNECT_PLUGIN_INSTALL_IMAGE} as build-plugins

## Installation of additional Kafka Connect Convertes to be delivered with the image
## Retries are required, because sometimes confluent hub cli does not find the connector
ARG CONFLUENT_VERSION=7.7.1
RUN until \
    connect-plugin-install \
      --confluent-hub-ids "confluentinc/kafka-connect-json-schema-converter:${CONFLUENT_VERSION}"; \
    do echo "Retry"; done
RUN until \
    connect-plugin-install \
      --confluent-hub-ids "confluentinc/kafka-connect-protobuf-converter:${CONFLUENT_VERSION}"; \
    do echo "Retry"; done
RUN until \
    connect-plugin-install \
      --confluent-hub-ids "confluentinc/kafka-connect-avro-converter:${CONFLUENT_VERSION}"; \
    do echo "Retry"; done

## Installation of additional Kafka Connect SMTs to be delivered with the image
RUN until \
    connect-plugin-install \
      --confluent-hub-ids "confluentinc/connect-transforms:1.6.1"; \
    do echo "Retry"; done


#################################
# Build Service Loader Support  #
#################################
FROM --platform=$BUILDPLATFORM ${OPENJDK_JDK_IMAGE} as build-service-loader-support
LABEL maintainer="code@uweeisele.eu"

WORKDIR /home/appuser
COPY --chown=appuser:appuser ./src ./src

WORKDIR /home/appuser/src/kafka-connect-json-schema-converter-sl
RUN ./mvnw clean package

WORKDIR /home/appuser/src/kafka-connect-protobuf-converter-sl
RUN ./mvnw clean package

WORKDIR /home/appuser/src/kafka-connect-avro-converter-sl
RUN ./mvnw clean package

WORKDIR /home/appuser/src/connect-transforms-sl
RUN ./mvnw clean package


#################################
# Build Kafka Connect (Image)   #
#################################
FROM --platform=$TARGETPLATFORM ${KAFKA_CONNECT_BASE_IMAGE}
LABEL maintainer="code@uweeisele.eu"

COPY --chown=appuser:appuser --from=build-plugins /opt/apache/kafka/plugins /opt/apache/kafka/plugins

COPY --chown=appuser:appuser --from=build-service-loader-support /home/appuser/src/kafka-connect-json-schema-converter-sl/target/*.jar /opt/apache/kafka/plugins/confluentinc-kafka-connect-json-schema-converter/lib/
COPY --chown=appuser:appuser --from=build-service-loader-support /home/appuser/src/kafka-connect-protobuf-converter-sl/target/*.jar /opt/apache/kafka/plugins/confluentinc-kafka-connect-protobuf-converter/lib/
COPY --chown=appuser:appuser --from=build-service-loader-support /home/appuser/src/kafka-connect-avro-converter-sl/target/*.jar /opt/apache/kafka/plugins/confluentinc-kafka-connect-avro-converter/lib/
COPY --chown=appuser:appuser --from=build-service-loader-support /home/appuser/src/connect-transforms-sl/target/*.jar /opt/apache/kafka/plugins/confluentinc-connect-transforms/lib/