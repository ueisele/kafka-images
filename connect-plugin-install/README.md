# Container Image for Connect Install Tools

The Container image contains tools for installing Connect plugins like Connectors to the [ueisele/apache-kafka-connect-base](https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect-base), [ueisele/apache-kafka-connect](https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect) and [ueisele/apache-kafka-connect-standalone](https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect-standalone) images.

The [connect/Containerfile](https://gitlab.com/ueisele/kafka-images/-/blob/main/connect/Containerfile) can be used as an example.

The Container images are available on Docker Hub repository [ueisele/apache-kafka-connect-plugin-install](https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect-plugin-install), and the source files for the images are available on GitLab repository [ueisele/kafka-images](https://gitlab.com/ueisele/kafka-images).

## Image

The Container images are based on [ueisele/zulu-openjdk-micro](https://hub.docker.com/repository/docker/ueisele/zulu-openjdk-micro) with _JRE_ installed (e.g. _21-jre_). 

The OpenJDK image in turn is based on [AlmaLinux 9 Micro](https://hub.docker.com/r/almalinux/9-micro).

As OpenJDK [Azul Zulu](https://www.azul.com/downloads/?package=jdk) is used.
Azul Zulu builds of OpenJDK are fully tested and TCK compliant builds of OpenJDK.


## Kafka Connect Plugin Installation

The Container image supports installation of Kafka Connect plugins like connectors during startup with multiple methods.

The plugins are installed by default to the `/opt/apache/kafka/plugins` directory. This can be changed with the environment variable `PLUGIN_INSTALL_DIR`.

Install plugin from Confluent Hub:

```bash
connect-plugin-install --confluent-hub-ids "confluentinc/kafka-connect-json-schema-converter:7.7.0"
```

Install plugins from URL:

```bash
connect-plugin-install --urls \
  https://github.com/castorm/kafka-connect-http/releases/download/v0.8.11/castorm-kafka-connect-http-0.8.11.zip \
  https://github.com/RedHatInsights/expandjsonsmt/releases/download/0.0.7/kafka-connect-smt-expandjsonsmt-0.0.7.tar.gz
```

Install additional libraries to directory:

```bash
connect-plugin-install \
  --confluent-hub-ids \
      confluentinc/kafka-connect-avro-converter:7.7.1 \
  --lib-urls \
      confluentinc-kafka-connect-avro-converter/lib=https://repo1.maven.org/maven2/com/google/guava/guava/32.0.1-jre/guava-32.0.1-jre.jar \
      confluentinc-kafka-connect-avro-converter/lib=https://repo1.maven.org/maven2/com/google/guava/failureaccess/1.0.1/failureaccess-1.0.1.jar
```

## Build

In order to create your own Container image clone the [ueisele/kafka-image](https://gitlab.com/ueisele/kafka-images) Git repository and run the build command:

```bash
git clone https://gitlab.com/ueisele/kafka-images.git
cd kafka-images
connect-plugin-install/build.sh --build
```

### Build Options

The `connect-plugin-install/build.sh` script provides the following options:

`Usage: connect-plugin-install/build.sh [--build] [--push] [--registry docker.io] [--user ueisele] [--archs amd64,arm64] [--openjdk-release 21] [--openjdk-version 21]`

## License 

This Container image is licensed under the [Apache 2 license](https://gitlab.com/ueisele/kafka-images/-/blob/main/LICENSE).