#!/usr/bin/env bash
set -e
BUILD_LIB_SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
BUILDTOOLS_DIR="${BUILD_LIB_SCRIPT_DIR}/../buildtools"
source "${BUILDTOOLS_DIR}/env"
source "${BUILDTOOLS_DIR}/base-image-lib.sh"

function _build_image_connect_plugin_install() {
  local id="${1}"
  local arch="${2}"
  local alma_release="${3}"
  local base_image_name="${4}"
  local image_name="${5}"

  system_init "${id}" "${alma_release}" "${arch}"
  target_init "${id}" "${base_image_name}"
  target_bind_root_to_system_build "${id}"
  system_run_install_packages "${id}" "grep" "sed" "tar" "gzip" "unzip" "curl" "ca-certificates"
  target_set_config "${id}" --user=root
  target_run "${id}" "mkdir -p /opt/apache/kafka/docker /opt/apache/kafka/plugins && chown appuser:appuser -R /opt/apache"
  target_copy_from_host_as_user "${id}" "${BUILD_LIB_SCRIPT_DIR}/include/opt/apache/kafka/docker" "/opt/apache/kafka/docker" "appuser"
  target_run "${id}" "mkdir -p /opt/confluent/confluent-hub-client/bin && chown appuser:appuser -R /opt/confluent"
  system_run_in_target "${id}" "curl -s -L http://client.hub.confluent.io/confluent-hub-client-latest.tar.gz | tar -xvz -C opt/confluent/confluent-hub-client"
  target_add_path "${id}" "/opt/confluent/confluent-hub-client/bin:/opt/apache/kafka/docker"
  target_set_config "${id}" --user=appuser
  target_commit "${id}" "${image_name}"
}

function build_image_connect_plugin_install() {
  local arch="${1:?"Require architecture as first parameter ('amd64' or 'arm64')!"}"
  local alma_release="${2:?"Require AlamaLinux release as second parameter (e.g. 9.4)!"}"
  local base_image_name="${3:?"Require base container image name as third parameter!"}"
  local image_name="${4:?"Require container image name as forth parameter (e.g. connect-plugin-install:latest)!"}"

  local id
  id="connect-plugin-install-${arch}-$(date +%s)"

  set +e # do not abort on errors
  _build_image_connect_plugin_install "${id}" "$@"
  local return_code=$?
  cleanup "${id}"
  set -e # abort on errors again
  return ${return_code}
}
