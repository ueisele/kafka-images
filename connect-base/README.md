# Container Image for Apache Kafka Connect (Base)

Container image for running the [Open Source version of Apache Kafka Connect](https://github.com/apache/kafka/) in distributed mode.

The Kafka distribution included in the Container image is built directly from [source](https://github.com/apache/kafka/).
It is a base image and therefore has no additional Cli tools and Kafka Connect plugins installed.

The Container images are available on Docker Hub repository [ueisele/apache-kafka-connect-base](https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect-base), and the source files for the images are available on GitLab repository [ueisele/kafka-images](https://gitlab.com/ueisele/kafka-images).

## Most Recent Tags

Most recent tags for `RELEASE` builds:

* `3.8.1`, `3.8.1-zulu21`, `3.8.1-zulu21.0.5`, `3.8.1-zulu21-alma9.5`, `3.8.1-zulu21.0.5-alma9.5-20241118`
* `3.9.0`, `3.9.0-zulu21`, `3.9.0-zulu21.0.5`, `3.9.0-zulu21-alma9.5`, `3.9.0-zulu21.0.5-alma9.5-20241118`

Most recent tags for `SNAPSHOT` builds:

* `4.0.0-SNAPSHOT`, `4.0.0-SNAPSHOT-zulu21`, `4.0.0-SNAPSHOT-zulu21.0.5`, `4.0.0-SNAPSHOT-zulu21-alma9.5`, `4.0.0-SNAPSHOT-zulu21.0.5-alma9.5-20241118`

Additionally, a tag with the associated Git-Sha of the built Apache Kafka distribution is always published as well, e.g. `ueisele/apache-kafka-connect-base:4.0.0-SNAPSHOT-gfd9de50-zulu21.0.5-alma9.5-20241118`.

## Image

The Container images are based on [ueisele/zulu-openjdk-micro](https://hub.docker.com/repository/docker/ueisele/zulu-openjdk-micro) with _JRE_ installed (e.g. _21-jre_). 

The OpenJDK image in turn is based on [AlmaLinux 9 Micro](https://hub.docker.com/r/almalinux/9-micro).

As OpenJDK [Azul Zulu](https://www.azul.com/downloads/?package=jdk) is used.
Azul Zulu builds of OpenJDK are fully tested and TCK compliant builds of OpenJDK.

## Quick Start

In the following section you find some simple examples to run Apache Kafka Connect.

First create a Container network:
```bash
podman network create quickstart-kafka-connect
```

Now, start a single Kafka instance: 

```bash
podman run -d --name kafka --net quickstart-kafka-connect -p 9092:9092 ueisele/apache-kafka-server-standalone:3.9.0
```

In order to run Apache Kafka Connect with a single instance run the following command:

```bash
podman run -d --name kafka-connect --net quickstart-kafka-connect -p 8083:8083 -p 9464:9464 \
    -e CONNECT_BOOTSTRAP_SERVERS=kafka:9092 \
    -e CONNECT_REST_ADVERTISED_HOST_NAME=localhost \
    -e CONNECT_REST_PORT=8083 \
    -e CONNECT_GROUP_ID=quickstart-kafka-connect \
    -e CONNECT_CONFIG_STORAGE_TOPIC=quickstart-kafka-connect-config \
    -e CONNECT_OFFSET_STORAGE_TOPIC=quickstart-kafka-connect-offsets \
    -e CONNECT_STATUS_STORAGE_TOPIC=quickstart-kafka-connect-status \
    -e CONNECT_CONFIG_STORAGE_REPLICATION_FACTOR=1 \
    -e CONNECT_OFFSET_STORAGE_REPLICATION_FACTOR=1 \
    -e CONNECT_STATUS_STORAGE_REPLICATION_FACTOR=1 \
    -e CONNECT_KEY_CONVERTER=org.apache.kafka.connect.json.JsonConverter \
    -e CONNECT_KEY_CONVERTER_SCHEMAS_ENABLE=false \
    -e CONNECT_VALUE_CONVERTER=org.apache.kafka.connect.json.JsonConverter \
    -e CONNECT_VALUE_CONVERTER_SCHEMAS_ENABLE=false \
    ueisele/apache-kafka-connect-base:3.9.0
```

You find additional examples in [examples/connect-standalone/](https://gitlab.com/ueisele/kafka-images/-/blob/main/examples/connect-standalone/):

* [examples/connect-standalone/sink-datagen/compose.yaml](https://gitlab.com/ueisele/kafka-images/-/blob/main/examples/connect-standalone/sink-datagen/compose.yaml)
* [examples/connect-standalone/source-http-json/compose.yaml](https://gitlab.com/ueisele/kafka-images/-/blob/main/examples/connect-standalone/source-http-json/compose.yaml)
* [examples/connect-standalone/source-http-avro/compose.yaml](https://gitlab.com/ueisele/kafka-images/-/blob/main/examples/connect-standalone/source-http-avro/compose.yaml)

## Configuration

For the Apache Kafka Connect ([ueisele/apache-kafka-connect-base](https://hub.docker.com/repository/registry-1.docker.io/ueisele/apache-kafka-connect-base/)) image, convert the [Apache Kafka Connect configuration properties](https://kafka.apache.org/documentation/#connectconfigs) as below and use them as environment variables:

* Prefix with CONNECT_.
* Convert to upper-case.
* Replace a period (.) with a single underscore (_).
* Replace a dash (-) with double underscores (__).
* Replace an underscore (_) with triple underscores (___).

The configuration is fully compatible with the [Confluent Docker images](https://docs.confluent.io/platform/current/installation/docker/config-reference.html#kconnect-long-configuration).

The configuration mechanism supports [`Go Template`](https://pkg.go.dev/text/template) for environment variable values.
The templating is done by [`godub`](https://github.com/ueisele/go-docker-utils) and therefore provides its [template functions](https://github.com/ueisele/go-docker-utils#template-functions). 

Example which uses `ipAddress` function to determin the IPv4 address of the first network interface:

```properties
CONNECT_REST_ADVERTISED_HOST_NAME="{{ ipAddress \"prefer\" \"ipv4\" 0 }}"
```
### Required Configuration

The minimum required worker configuration is:

* `CONNECT_BOOTSTRAP_SERVERS` which defines the the Kafka bootstrap servers
* `CONNECT_KEY_CONVERTER` and `CONNECT_VALUE_CONVERTER` which define the converters used for key and value.
* `CONNECT_GROUP_ID` which identifies the Connect cluster group this Worker belongs to.
* `CONNECT_CONFIG_STORAGE_TOPIC`, `CONNECT_OFFSET_STORAGE_TOPIC` and `CONNECT_STATUS_STORAGE_TOPIC` which define the names of the topics where connector tasks, configuration, offsets and status updates are stored. This names must be unique per Connect cluster.
* `CONNECT_REST_ADVERTISED_HOST_NAME` which defines the hostname that will be given out to other Workers to connect to. You should set this to a value that is resolvable by all containers.

### Logging

The logging configuration can be adjusted with the following environment variables:

* `CONNECT_LOG4J_PATTERN` sets the logging pattern (default: `[%d] (%t) %p %m (%c)%n`)
* `CONNECT_LOG4J_ROOT_LOGLEVEL` sets the root log level (default: `INFO`)
* `CONNECT_LOG4J_LOGGERS` is a comma separated list of logger and log level key-value pairs (default: `org.reflections=ERROR,org.apache.zookeeper=ERROR,org.I0Itec.zkclient=ERROR`)

### JMX

Remote JMX can be enabled with the following environment variables:

```properties
KAFKA_JMX_PORT=6001
KAFKA_JMX_HOSTNAME=localhost
```

### Prometheus

The image contains the [Prometheus JMX Exporter JavaAgent](https://github.com/prometheus/jmx_exporter). In the default configuration, this exports metrics via a [Prometheus](https://prometheus.io/) endpoint on port _9464_.
As configuration for the Kafka metrics, the file [default.yaml](https://gitlab.com/ueisele/kafka-images/-/blob/main/connect-base/include/opt/apache/kafka/javaagents/prometheus/configs/default.yaml) is used. 

The Prometheus JMX Exporter JavaAgent can be configured via environment variables:

> #### `PROMETHEUS_JAVAAGENT_ENABLED`
> Can be used to disable the Prometheus JMX Exporter JavaAgent.
> *   Type: `Boolean`
> *   Default: `true`
>
> #### `PROMETHEUS_EXPORTER_CONFIG`
> To provide your own metric definitions, create a [YAML configuration file](https://github.com/prometheus/jmx_exporter), and specify their location.
> The provided [default.yaml](https://gitlab.com/ueisele/kafka-images/-/blob/main/connect-base/include/opt/apache/kafka/javaagents/prometheus/configs/default.yaml) can be used as an example.
> *   Type: `String`
> *   Default: `/opt/apache/kafka/javaagents/prometheus/configs/default.yaml`
>
> #### `PROMETHEUS_EXPORTER_PORT`
> The port on which the prometheus metrics are provided.
> *   Type: `Integer`
> *   Default: `9464`
>

### Debugging

In order to debug Kafka Connect, set the following environment variable:

```properties
KAFKA_DEBUG=y
```

In addition you can configure the behavior with the following environment variables:

```properties
JAVA_DEBUG_PORT=5005
DEBUG_SUSPEND_FLAG=y
```

### Kafka Connect Plugin Installation

You can install Kafka Connect plugins with multi stage builds. The [connect/Containerfile](https://gitlab.com/ueisele/kafka-images/-/blob/main/connect/Containerfile) can be used as an example.

If you want to be able to directly install plugins in the image, use [ueisele/apache-kafka-connect-dev](https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect-dev) instead.

## Build

In order to create your own Container image for Apache Kafka Connect clone the [ueisele/kafka-image](https://gitlab.com/ueisele/kafka-images) Git repository and run the build command:

```bash
git clone https://gitlab.com/ueisele/kafka-images.git
cd kafka-images
connect-base/build.sh --build --tag 3.9.0 --openjdk-release 21
```

To create an image with a specific OpenJDK version use the following command:

```bash
connect-base/build.sh --build --tag 3.9.0 --openjdk-release 21 --openjdk-version 21.0.5
```

By default Apache Kafka 3.9.0 does not support Java 21. In order to build Apache Kafka 3.9.0 with Java 21, the Gradle configuration is patched with [patch/3.9.0-21.patch]().

```bash
server/build.sh --build --tag 3.9.0 --openjdk-release 21 --patch
```

To build the most recent `SNAPSHOT` of Apache Kafka 3.9.0 with Java 21, run:

```bash
server/build.sh --build --branch trunk --openjdk-release 21
```

### Build Options

The `connect-base/build.sh` script provides the following options:

`Usage: connect-base/build.sh [--build] [--push] [--registry docker.io] [--user ueisele] [--archs amd64,arm64] [--github-repo apache/kafka] [--commit-sha 60e8456] [--tag 3.9.0] [--branch trunk] [--pull-request 9999] [--openjdk-release 21] [--openjdk-version 21.0.5] [--patch 3.9.0-21.patch] [--cache-maven <m2-directory>] [--cache-gradle <gradle-directory>]`

## License 

This Container image is licensed under the [Apache 2 license](https://gitlab.com/ueisele/kafka-images/-/blob/main/LICENSE).