#!/usr/bin/env bash
# Creation of base image is based on AlmaLinux 9 Micro Image
# https://github.com/AlmaLinux/docker-images/blob/main/dockerfiles/al9/Dockerfile.micro
set -e
source "$(dirname ${BASH_SOURCE[0]})/env"

function system_init() {
  local system_build_ctr="$(system_build_ctr $1)"
  local release="${2:?"Require AlmaLinux release as second parameter, e.g. 9.4!"}"
  local arch="${3:?"Require arch as third parameter. e.g. amd64!"}"
  # https://hub.docker.com/_/almalinux
  buildah from --name "${system_build_ctr}" --platform "linux/${arch}" --pull "docker.io/almalinux:${release}"
  buildah config --label os_id=almalinux --label "os_release=${release}" "${system_build_ctr}"
}

function target_init() {
  local system_build_ctr="$(system_build_ctr "$1")"
  local target_ctr="$(target_ctr "$1")"
  local parent_image="${2:?"Require parent image name as second parameter!"}"
  local arch="$(buildah inspect --format '{{.OCIv1.Architecture}}' "${system_build_ctr}")"
  buildah from --name "${target_ctr}" --platform "linux/${arch}" --pull "${parent_image}"
  buildah config --author='code@uweeisele.eu' "${target_ctr}"
  buildah config --env LANG=C.utf8 "${target_ctr}"
}

# 'target_bind_root_to_system_build' and 'target_copy_root_from_system_build' are mutually exclusive
function target_bind_root_to_system_build() {
  local system_build_ctr="$(system_build_ctr "$1")"
  local target_ctr="$(target_ctr "$1")"
  local target_mount="$(buildah unshare buildah mount "${target_ctr}")"
  buildah config --label target_mount="${target_mount}" "${system_build_ctr}"
}

function system_run_install_packages() {
  local id="${1:?"Required unique id as first parameter!"}"
  local system_build_ctr="$(system_build_ctr ${id})"
  local os_release="$(buildah inspect --format '{{.OCIv1.Config.Labels.os_release}}' "${system_build_ctr}")"
  local packages="${*:2}"
  system_run "${id}" "\
      mkdir -p \"${SYSTEM_BUILD_ROOT}\" && \
      dnf install --installroot \"${SYSTEM_BUILD_ROOT}\" ${packages} \
        --releasever ${os_release} --setopt install_weak_deps=false --nodocs -y && \
      dnf --installroot \"${SYSTEM_BUILD_ROOT}\" clean all && \
      rm -rf \"${SYSTEM_BUILD_ROOT}/var/cache/dnf\" \"${SYSTEM_BUILD_ROOT}/var/log/dnf*\" \"${SYSTEM_BUILD_ROOT}/var/lib/dnf\" \"${SYSTEM_BUILD_ROOT}/var/log/yum.*\" \
    "
}

function system_run_install_root() {
  local id="${1:?"Required unique id as first parameter!"}"
  system_run_install_packages "${id}" coreutils-single glibc-minimal-langpack
  system_run "${id}" "\
    /bin/date +%Y%m%d_%H%M > \"${SYSTEM_BUILD_ROOT}/etc/BUILDTIME\" && \
    echo 'LANG=\"C.utf8\"' >  \"${SYSTEM_BUILD_ROOT}/etc/locale.conf\" && \
    echo 'container' > \"${SYSTEM_BUILD_ROOT}/etc/dnf/vars/infra\" && \
    rm -f \"${SYSTEM_BUILD_ROOT}/etc/machine-id\" && \
    touch \"${SYSTEM_BUILD_ROOT}/etc/machine-id\" && \
    touch \"${SYSTEM_BUILD_ROOT}/etc/.pwd.lock\" && \
    chmod 600 \"${SYSTEM_BUILD_ROOT}/etc/.pwd.lock\" && \
    rm -rf \"${SYSTEM_BUILD_ROOT}/usr/share/locale/en*\" \"${SYSTEM_BUILD_ROOT}/boot\" \"${SYSTEM_BUILD_ROOT}/dev/null\" \"${SYSTEM_BUILD_ROOT}/var/log/hawkey.log\" && \
    echo '0.0 0 0.0' > \"${SYSTEM_BUILD_ROOT}/etc/adjtime\" && \
    echo '0' >> \"${SYSTEM_BUILD_ROOT}/etc/adjtime\" && \
    echo 'UTC' >> \"${SYSTEM_BUILD_ROOT}/etc/adjtime\" && \
    echo 'KEYMAP=\"us\"' > \"${SYSTEM_BUILD_ROOT}/etc/vconsole.conf\" && \
    echo 'FONT=\"eurlatgr\"' >> \"${SYSTEM_BUILD_ROOT}/etc/vconsole.conf\" && \
    mkdir -p \"${SYSTEM_BUILD_ROOT}/run/lock\" && \
    cd \"${SYSTEM_BUILD_ROOT}/etc\" && \
    ln -sf ../usr/share/zoneinfo/UTC localtime 
  "
}

function system_run_in_target() {
  local id="${1:?"Required unique id as first parameter!"}"
  system_run "${id}" "\
      mkdir -p \"${SYSTEM_BUILD_ROOT}\" && \
      cd \"${SYSTEM_BUILD_ROOT}\" &&
      ${*:2} \
    "
}

function system_run_useradd() {
  local id="${1:?"Required unique id as first parameter!"}"
  local user_name="${2:?"Require user name as second parameter!"}"
  system_run "${id}" "\
      useradd --root \"${SYSTEM_BUILD_ROOT}\" --no-log-init --create-home --user-group --shell /bin/bash ${user_name}; \
    "
}

function system_run() {
  local system_build_ctr="$(system_build_ctr "$1")"
  local target_mount="$(buildah inspect --format '{{.OCIv1.Config.Labels.target_mount}}' "${system_build_ctr}")"
  buildah run \
    $([ "${target_mount}" != "<no value>" ] && echo --volume "${target_mount}:${SYSTEM_BUILD_ROOT}:rw,z" || true) \
    "${system_build_ctr}" \
    /bin/bash -c "${@:2}"
}

function target_set_user_and_workdir_to_home() {
  local id="${1:?"Required unique id as first parameter!"}"
  local user="${2:?"Require user name as second parameter!"}"
  target_set_config "${id}" --user="${user}" --workingdir="/home/${user}"
}

function target_set_cmd_bash() {
  local id="${1:?"Required unique id as first parameter!"}"
  target_set_config "${id}" --cmd /bin/bash
}

function target_set_config() {
  local target_ctr="$(target_ctr "$1")"
  buildah config "${@:2}" "${target_ctr}"
}

# 'target_bind_root_to_system_build' and 'target_copy_root_from_system_build' are mutually exclusive
function target_copy_root_from_system_build() {
  local system_build_ctr="$(system_build_ctr "$1")"
  local target_ctr="$(target_ctr "$1")"
  buildah copy --from "${system_build_ctr}" "${target_ctr}" "${SYSTEM_BUILD_ROOT}" /
}

function target_copy_from_host_as_user() {
  local target_ctr="$(target_ctr "$1")"
  local source_dir="${2:?"Require source dir as second paremeter!"}"
  local target_dir="${3:?"Require source dir as third paremeter!"}"
  local user="${4:?"Require user as forth paremeter!"}"
  buildah copy --chown "${user}" "${target_ctr}" "${source_dir}" "${target_dir}"
}

function target_run() {
  local target_ctr="$(target_ctr "$1")"
  buildah run \
    "${target_ctr}" \
    /bin/bash -c "${@:2}"
}

function target_add_path() {
  local path="${2:?"Requires path as second parameter!"}"
  local current_path="$(target_run "$1" "echo \${PATH}" )"
  target_set_config "$1" --env "PATH=${path}:${current_path}"
}

function target_commit() {
  local target_ctr="$(target_ctr "$1")"
  local image_name="${2:?"Requires image name as first parameter!"}"
  buildah commit "${target_ctr}" "${image_name}"
  echo "Built image ${image_name}"
}

function cleanup() {
  local system_build_ctr="$(system_build_ctr "$1")"
  local target_ctr="$(target_ctr "$1")"
  buildah umount "${target_ctr}" 1>/dev/null 2>&1 || true
  buildah rm "${target_ctr}" 1>/dev/null 2>&1 || true
  buildah rm "${system_build_ctr}" 1>/dev/null 2>&1 || true
}

### Examples ###

## example: almalinux/9-micro as parent
function example_parent_alma() {
  local id="example-$(date +%s)"
  system_init "${id}" "9.2" "amd64"
  target_init "${id}" "docker.io/almalinux/9-micro:9.2-20230512"
  target_bind_root_to_system_build "${id}"
  system_run_install_packages "${id}" "ca-certificates" "hostname"
  target_commit "${id}" "example-alma-a:${id}"
  system_run_useradd "${id}" "appuser"
  target_set_user_and_workdir_to_home "${id}" "appuser"
  target_set_cmd_bash "${id}"
  target_commit "${id}" "example-alma:${id}"
  cleanup "${id}"
}

## example: from scratch
function example_from_scratch() {
  local id="example-$(date +%s)"
  system_init "${id}" "9.2" "amd64"
  target_init "${id}" "scratch"
  system_run_install_root "${id}"
  system_run_install_packages "${id}" "ca-certificates" "hostname"
  system_run_useradd "${id}" "appuser"
  target_copy_root_from_system_build "${id}"
  target_set_user_and_workdir_to_home "${id}" "appuser"
  target_set_cmd_bash "${id}"
  target_commit "${id}" "example-scratch:${id}"
  cleanup "${id}"
}
