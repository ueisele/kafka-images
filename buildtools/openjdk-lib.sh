#!/usr/bin/env bash
set -e
OPENJDK_LIB_SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
source "${OPENJDK_LIB_SCRIPT_DIR}/env"
source "${OPENJDK_LIB_SCRIPT_DIR}/base-image-lib.sh"

function system_run_install_openjdk() {
  local id="${1:?"Required unique id as first parameter!"}"
  local openjdk_version="${2:?"Require OpenJDK version as second parameter, e.g. 21.0.5!"}"
  local openjdk_variant="${3:?"Require OpenJDK variant as third parameter, can be 'jre' or 'jdk'!"}"
  local openjdk_release="${openjdk_version%.*.*}"
  local zulu_package="zulu${openjdk_release}-${openjdk_variant}-headless-${openjdk_version}"
  system_run_install_packages "${id}" https://cdn.azul.com/zulu/bin/zulu-repo-1.0.0-1.noarch.rpm
  system_run_install_packages "${id}" "${zulu_package}"
}

function target_set_java_home() {
  local id="${1:?"Required unique id as first parameter!"}"
  local openjdk_version="${2:?"Require OpenJDK version as second parameter, e.g. 21.0.5!"}"
  local openjdk_release="${openjdk_version%.*.*}"
  target_set_config "${id}" --env "JAVA_HOME=/usr/lib/jvm/zulu${openjdk_release}"
}
