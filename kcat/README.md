# Container Image for Kcat

Check out [edenhill/kcat](https://github.com/edenhill/kcat) for more information.

The Container images are available in the following repositories on Docker Hub:

* [ueisele/kcat](https://hub.docker.com/repository/docker/ueisele/kcat)

## Most Recent Tags

* `1.7.1`, `1.7.1-librdkafka2.6.1`, `1.7.1-librdkafka2.6.1-alma9.5`, `1.7.1-librdkafka2.6.1-alma9.5-20241118`

Most recent tags for `SNAPSHOT` builds:

* `1.7.1-11-gab6ce81`, `1.7.1-11-gab6ce81-librdkafka2.6.1`, `1.7.1-11-gab6ce81-librdkafka2.6.1-alma9.5`, `1.7.1-11-gab6ce81-librdkafka2.6.1-alma9.5-20241118`

## Image

The Container image is based on [AlmaLinux 9 Minimal](https://hub.docker.com/r/almalinux/9-minimal).

## Usage

To run a container of your choice, use commands below as an example.

```bash
podman run --rm ueisele/kcat:1.7.1
```

## Build

In order to create your own Kcat Container image clone the [ueisele/kafka-image](https://gitlab.com/ueisele/kafka-images) Git repository and run the build command for the Kcat image:

```bash
git clone https://gitlab.com/ueisele/kafka-images.git
cd kafka-images
kcat/build.sh --build --user ueisele --tag 1.7.1
```
## License 

This Container image is licensed under the [Apache 2 license](https://gitlab.com/ueisele/kafka-images/-/blob/main/LICENSE).