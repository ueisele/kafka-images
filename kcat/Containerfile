ARG ALMA9_VERSION
FROM docker.io/almalinux/9-minimal:${ALMA9_VERSION} as build
LABEL maintainer="code@uweeisele.eu"

RUN microdnf update -y \
    && microdnf clean all \
    && rm -rf /tmp/* && rm -rf /var/lib/rpm
RUN microdnf install -y \
      git \
    && microdnf clean all \
    && rm -rf /tmp/* && rm -rf /var/lib/rpm
RUN microdnf install -y \
      epel-release \ 
    && microdnf install --enablerepo=crb -y \
        which wget python3 diffutils \
        cmake pkg-config gcc-c++ libtool \
        openssl-devel openldap-devel cyrus-sasl-lib cyrus-sasl-devel libssh-devel \
        krb5-devel brotli-devel libidn2-devel libnghttp2-devel curl-devel \
        libpsl-devel lz4-devel snappy-devel zlib-devel libzstd-devel rapidjson-devel \
    && microdnf clean all \
    && rm -rf /tmp/* && rm -rf /var/lib/rpm

WORKDIR /usr/src/kcat

ARG KCAT_GIT_REPO=https://github.com/edenhill/kcat.git
ARG KCAT_GIT_REFSPEC
RUN git clone https://github.com/edenhill/kcat.git . \
  && git checkout ${KCAT_GIT_REFSPEC}
ARG LIBRDKAFKA_VERSION=2.5.0
RUN LIBRDKAFKA_VERSION=v${LIBRDKAFKA_VERSION} ./bootstrap.sh


ARG ALMA9_VERSION
FROM docker.io/almalinux/9-minimal:${ALMA9_VERSION}
LABEL maintainer="code@uweeisele.eu"

ENV LANG="C.UTF-8"

RUN microdnf install --enablerepo=crb -y \
      openssl libssh cyrus-sasl cyrus-sasl-gssapi \
      brotli libidn2 libnghttp2 libpsl \
      lz4 snappy zlib libzstd \
    && microdnf clean all \
    && rm -rf /tmp/* && rm -rf /var/lib/rpm

RUN microdnf -y install shadow-utils \
    && useradd --no-log-init --create-home --shell /bin/bash appuser \
    && microdnf remove -y shadow-utils \
    && microdnf clean all \
    && rm -rf /tmp/* && rm -rf /var/lib/rpm

USER appuser
WORKDIR /home/appuser

COPY --from=build --chown=appuser:appuser /usr/src/kcat/kcat /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/kcat"]

ARG KCAT_GIT_REPO=https://github.com/edenhill/kcat.git \
    KCAT_GIT_REFSPEC \
    KCAT_BUILD_GIT_REFSPEC=${KCAT_GIT_REFSPEC} \
    KCAT_VERSION=${KCAT_GIT_REFSPEC} \
    LIBRDKAFKA_VERSION
LABEL kcat.git.repo="${KCAT_GIT_REPO}" \
    kcat.git.refspec="${KCAT_GIT_REFSPEC}" \
    kcat.build.git.refspec="${KCAT_BUILD_GIT_REFSPEC}" \
    kcat.version="${KCAT_VERSION}" \
    librdkafka.version="${LIBRDKAFKA_VERSION}"