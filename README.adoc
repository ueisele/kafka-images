= Apache Kafka Container Images

Container image for running the link:https://github.com/apache/kafka/[Open Source version of Apache Kafka]. It offers support for running Kafka in link:https://github.com/apache/kafka/blob/3.3.1/config/kraft/README.md[KRaft] mode as well as in ZooKeeper mode.

The Kafka distribution included in the Container image is built directly from link:https://github.com/apache/kafka/[source].

The Container images are available on the following Docker Hub repositories:

* link:https://hub.docker.com/repository/docker/ueisele/apache-kafka-server[ueisele/apache-kafka-server]
* link:https://hub.docker.com/repository/docker/ueisele/apache-kafka-server-standalone[ueisele/apache-kafka-server-standalone]
* link:https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect-base[ueisele/apache-kafka-connect-base]
* link:https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect[ueisele/apache-kafka-connect]
* link:https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect-dev[ueisele/apache-kafka-connect-dev]
* link:https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect-standalone[ueisele/apache-kafka-connect-standalone]
* link:https://hub.docker.com/repository/docker/ueisele/apache-kafka-connect-standalone-dev[ueisele/apache-kafka-connect-standalone-dev]
* link:https://hub.docker.com/repository/docker/ueisele/kcat[ueisele/kcat]
* link:https://hub.docker.com/repository/docker/ueisele/cp-schema-registry[ueisele/cp-schema-registry]
* link:https://hub.docker.com/repository/docker/ueisele/cp-schema-registry-tools[ueisele/cp-schema-registry-tools]

== Quick Start

.To start Kafka in KRaft single mode just run: 
[source,bash]
----
podman run --rm -p 19092:19092 ueisele/apache-kafka-server-standalone:3.9.0
----

.To start Kafka in KRaft single mode with Ipv6 just run: 
[source,bash]
----
podman network create --ipv6 --subnet fd01::/80 kafka-standalone
podman run --rm -p 19092:19092 --net kafka-standalone -e STANDALONE_BROKER_IP_VERSION=ipv6 ueisele/apache-kafka-server-standalone:3.9.0
----

Hint: Instead of link:https://podman.io/[Podman], you can also use link:https://www.docker.com/[Docker]. Just replace _podman_ with _docker_.

Hint: If you use _Docker_, you can directly use port 9092 and bind it to any host port.
The reason is, that `<container-ip>:9092` is exposed, which is reachable if Docker is used.

== Build

The Container images are created with link:https://buildah.io/[Buildah] and implemented using link:https://docs.docker.com/engine/reference/builder/[Containerfile].

*Why Buildah instead of Docker?* Using _Buildah_ is different from building images with the _docker_ command. The most important one is that it requires no container runtime.
For more information see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/building_running_and_managing_containers/assembly_building-container-images-with-buildah_building-running-and-managing-containers[Building container images with Buildah].

*Containerfile or Dockerfile?* Both are exactly the same. The link:https://docs.docker.com/engine/reference/builder/[Dockerfile reference] describes the syntax which is valid for both. However, nowadays _Dockerfiles_ are not specific to _Docker_ anymore. It works also with other tools like link:https://buildah.io/[Buildah], link:https://podman.io/[Podman] or link:https://github.com/GoogleContainerTools/kaniko[kaniko].
In order to do justice to this differentiation, the term _Containerfile_ was introduced as a generalization as part of the link:https://github.com/containers/buildah/issues/1853[Buildah project].
It is important to note, that _Docker_ did not adopt the name _Containerfile_ until now.

== Compose Examples

In this section you find an overview about the Kafka Server and Kafka Connect examples.

Hint: The example setups are implemented as link:https://docs.docker.com/compose/compose-file/03-compose-file/[Compose file]. It is identical to a _Docker Compose file_ but has been officially renamed.

Hint: In the examples link:https://github.com/containers/podman-compose[podman-compose] command is used to execute the _Compose files_. It can be replaces with other tools compliant to the _Compose file_ spec like link:https://docs.docker.com/engine/reference/commandline/compose/[docker compose].

=== Kafka KRaft Mode Examples

.To start Kafka in KRaft single mode just run: 
[source,bash]
----
cd example/kraft/single-mode
podman-compose up
----

You find all examples in link:examples/kraft/[]:

* link:examples/kraft/single-mode/compose.yaml[]
* link:examples/kraft/cluster-shared-mode/compose.yaml[]
* link:examples/kraft/cluster-dedicated-mode/compose.yaml[]
* link:examples/kraft/cluster-mixed-mode/compose.yaml[]
* link:examples/kraft/cluster-ssl-pem/compose.yaml[]
* link:examples/kraft/cluster-ssl-jks/compose.yaml[]
* link:examples/kraft/cluster-ssl-pkcs12/compose.yaml[]

=== Kafka ZooKeeper Mode Examples

.To start Kafka in ZooKeeper mode just run: 
[source,bash]
----
cd example/zk/cluster
podman-compose up
----

You find additional examples in link:examples/zk/[]:

* link:examples/zk/cluster/compose.yaml[]

=== Connect Examples

.To start Kafka Connect in standalone mode just run: 
[source,bash]
----
cd example/connect-standalone/sing-datagen
podman-compose up
----

You find additional examples in link:examples/connect-standalone/[]:

* link:examples/connect-standalone/sink-datagen/compose.yaml[]
* link:examples/connect-standalone/source-http-json/compose.yaml[]
* link:examples/connect-standalone/source-http-avro/compose.yaml[]

== Build and Configuration

* OpenJDK base image: link:openjdk/minimal/[]
* OpenJDK Micro base image: link:openjdk/micro/[]
* Apache Kafka server image: link:server/[]
* Apache Kafka server standalone image: link:server-standalone/[]
* Apache Kafka Connect base image: link:connect-base/[]
* Apache Kafka Connect image: link:connect/[]
* Apache Kafka Connect standalone image: link:connect-standalone/[]
* Kcat image: link:kcat/[]
* Confluent Schema Registry image: link:cp-schema-registry/[]
* Confluent Schema Registry Tools image: link:cp-schema-registry-tools/[]

== Troubleshooting

=== Podman-Compose Support

It is important to note that at least link:https://github.com/containers/podman-compose[podman-compose] 1.0.4 is required. The reason is, that in the examples network alias are used.

If you do not have the required minimum version, you could install it manually:
[source,bash]
----
pip3 install --upgrade --target=${HOME}/Applications/podman-compose git+https://github.com/containers/podman-compose.git
cat > ~/.local/bin/podman-compose <<EOF
PODMAN_COMPOSE_PATH=${HOME}/Applications/podman-compose
PYTHONPATH=\${PODMAN_COMPOSE_PATH} \${PODMAN_COMPOSE_PATH}/bin/podman-compose \$@
EOF
chmod +x ~/.local/bin/podman-compose
----
