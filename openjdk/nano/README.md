# Container Image for Azul Zulu OpenJDK 

Azul Zulu builds of OpenJDK are fully tested and TCK compliant builds of OpenJDK.

Check out [Azul Zulu Overview](https://www.azul.com/downloads/?package=jdk) for more information.

The Container images are available in the following repositories on Docker Hub:

* [ueisele/zulu-openjdk-nano](https://hub.docker.com/repository/docker/ueisele/zulu-openjdk-nano)

## Most Recent Tags

* `23`, `23.0.1`, `23-alma9.5`, `23.0.1-alma9.5-20241118`
* `21`, `21.0.5`, `21-alma9.5`, `21.0.5-alma9.5-20241118`
* `17`, `17.0.13`, `17-alma9.5`, `17.0.13-alma9.5-20241118`

* `23-jre`, `23.0.1-jre`, `23-jre-alma9.5`, `23.0.1-jre-alma9.5-20241118`
* `21-jre`, `21.0.5-jre`, `21-jre-alma9.5`, `21.0.5-jre-alma9.5-20241118`
* `17-jre`, `17.0.13-jre`, `17-jre-alma9.5`, `17.0.13-jre-alma9.5-20241118`

## Image

The source files for the images are available on [GitLab](https://gitlab.com/ueisele/kafka-images).

The Container image has been built from scratch. As system builder, [AlmaLinux 9](https://hub.docker.com/_/almalinux) is used.

> The minimal image is a stripped-down image that uses the microdnf as package manager which uses libdnf and hence doesn't require Python. This image is 52% smaller in size (37MB download, 102MB expanded), contains a very limited package set. It is designed for applications that come with their own dependencies bundled (e.g. GO, NodeJS, Java).

Azul already provides container images with Azul Zulu OpenJDK at their [DockerHub repository](https://hub.docker.com/r/azul/zulu-openjdk-centos).
We decided to create our own image, because we wanted to use [AlmaLinux 9](https://hub.docker.com/_/almalinux) to build a minimalistic image.
In addition our image is smaller, than the original [Azul Zulu OpenJDK Container image](https://hub.docker.com/r/azul/zulu-openjdk-centos).

## Usage

To run a container of your choice, use commands below as an example.

For Azul Zulu OpenJDK 20, run:

```bash
podman run --rm docker.io/ueisele/zulu-openjdk-nano:21 java --version
```

## Build

In order to create your own Azul Zulu OpenJDK Container image clone the [ueisele/kafka-image](https://gitlab.com/ueisele/kafka-images) Git repository and run the build command for the OpenJDK image:

```bash
git clone https://gitlab.com/ueisele/kafka-images.git
cd kafka-images
openjdk/nano/build.sh --build --user ueisele --openjdk-release 21
```

To create an image with a specific OpenJDK version use the following command:

```bash
openjdk/nano/build.sh --build --user ueisele --openjdk-release 17 --openjdk-version 17.0.13
```

## License 

This Container image is licensed under the [Apache 2 license](https://gitlab.com/ueisele/kafka-images/-/blob/main/LICENSE).