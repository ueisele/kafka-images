# Container Image for Azul Zulu OpenJDK 

Azul Zulu builds of OpenJDK are fully tested and TCK compliant builds of OpenJDK.

Check out [Azul Zulu Overview](https://www.azul.com/downloads/?package=jdk) for more information.

The Container images are available in the following repositories on Docker Hub:

* [ueisele/zulu-openjdk-micro](https://hub.docker.com/repository/docker/ueisele/zulu-openjdk-micro)

## Most Recent Tags

* `23`, `23.0.1`, `23-alma9.5`, `23.0.1-alma9.5-20241118`
* `21`, `21.0.5`, `21-alma9.5`, `21.0.5-alma9.5-20241118`
* `17`, `17.0.13`, `17-alma9.5`, `17.0.13-alma9.5-20241118`

* `23-jre`, `23.0.1-jre`, `23-jre-alma9.5`, `23.0.1-jre-alma9.5-20241118`
* `21-jre`, `21.0.5-jre`, `21-jre-alma9.5`, `21.0.5-jre-alma9.5-20241118`
* `17-jre`, `17.0.13-jre`, `17-jre-alma9.5`, `17.0.13-jre-alma9.5-20241118`

## Image

The source files for the images are available on [GitLab](https://gitlab.com/ueisele/kafka-images).

The Container image is based on [AlmaLinux 9 Micro](https://hub.docker.com/r/almalinux/9-micro):

> The AlmaLinux micro image is a stripped down image, distributed without any package manager. Micro image uses the package manager on the underlying host to install packages, typically using Buildah, or Multi-stage builds with Podman. Micro image is 82% smaller then base image, 68% smaller then minimal image in size (13MB download, 36MB expanded). It is designed for applications that come with their own dependencies bundled (e.g. GO, NodeJS, Java). Since this image has only very few packages, it is more secured compare to other images.

Azul already provides container images with Azul Zulu OpenJDK at their [DockerHub repository](https://hub.docker.com/r/azul/zulu-openjdk-centos).
We decided to create our own image, because we wanted [AlmaLinux 9 Micro](https://hub.docker.com/r/almalinux/9-micro) as base for our image.
In addition our image is smaller, than the original [Azul Zulu OpenJDK Container image](https://hub.docker.com/r/azul/zulu-openjdk-centos).

## Usage

To run a container of your choice, use commands below as an example.

For Azul Zulu OpenJDK 21, run:

```bash
podman run --rm docker.io/ueisele/zulu-openjdk-micro:21 java --version
```

## Build

In order to create your own Azul Zulu OpenJDK Container image clone the [ueisele/kafka-image](https://gitlab.com/ueisele/kafka-images) Git repository and run the build command for the OpenJDK image:

```bash
git clone https://gitlab.com/ueisele/kafka-images.git
cd kafka-images
openjdk/micro/build.sh --build --user ueisele --openjdk-release 21
```

To create an image with a specific OpenJDK version use the following command:

```bash
openjdk/micro/build.sh --build --user ueisele --openjdk-release 17 --openjdk-version 17.0.13
```

If you are running in rootless mode, it may necessary, to run with `buildah unshare` command:

```bash
buildah unshare openjdk/micro/build.sh --build --openjdk-release 21
```

> Buildah and Podman have a special command, unshare. This command creates and enters the user namespace without creating or interacting with a container. It is actually fairly interesting to explore this mode to fully understand what the user namespace is doing. Executing the buildah unshare command will run a shell command in the namespaces running as root in the user namespace. Now you can run any command, including the buildah commands described above. Since these commands are already in the namespaces, the buildah mount command will work the same as it does in rootful mode. Everything happens inside of the namespaces, and the user gets what they expect. (https://www.redhat.com/sysadmin/buildah-unshare-command)

The reason why we need `buildah unshare` is, that `buildah mount ${target_ctr}` is used to build this image.
We create it, by mounting the root volume of `docker.io/almalinux/9-micro` to the host. The OpenJDK is installed with the help of the package manager of another container. With this approach, we can use `docker.io/almalinux/9-micro` and install tools to it without requiring a package manager in the image itself.

With `dnf`, you can easily specify a alternative root directory: `dnf install --installroot /mnt/sys-root coreutils-single glibc-minimal-langpack --releasever 9.5`

If you are running withing a container, ensure that you also set environment variables `BUILDAH_ISOLATION`:

```bash
export BUILDAH_ISOLATION=chroot
```

> Finally, we default the Buildah container to run with chroot isolation.  Setting the environment variable BUILDAH_ISOLATION tells Buildah to default to using chroot.  We don’t need to run with extra isolation because we are already running within a container. Having Buildah create its own namespace separated containers requires SYS_ADMIN privileges and requires us to relax SELinux and SECCOMP rules on the running container, defeating the purpose of running builds within a locked-down container. (https://developers.redhat.com/blog/2019/08/14/best-practices-for-running-buildah-in-a-container#setup)

```bash
podman run --rm -it \
  --security-opt label:disable \
  -v $(pwd):/work:z \
  quay.io/buildah/stable:latest \
  sudo -E -u build buildah unshare bash -c "/work/openjdk/micro/build.sh --build"
```

Hint: The image `quay.io/buildah/stable` has the variable `BUILDAH_ISOLATION=chroot` already set.

## License 

This Container image is licensed under the [Apache 2 license](https://gitlab.com/ueisele/kafka-images/-/blob/main/LICENSE).