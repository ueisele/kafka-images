#!/usr/bin/env bash
set -e
BUILD_LIB_SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
source "${BUILD_LIB_SCRIPT_DIR}/../env"

BUILDTOOLS_DIR="${BUILD_LIB_SCRIPT_DIR}/../../buildtools"
source "${BUILDTOOLS_DIR}/env"
source "${BUILDTOOLS_DIR}/base-image-lib.sh"
source "${BUILDTOOLS_DIR}/openjdk-lib.sh"

function _build_image_zulu_openjdk_micro() {
  local id="${1}"
  local arch="${2}"
  local openjdk_variant="${3}"
  local openjdk_version="${4}"
  local image_name="${5}"

  system_init "${id}" "${ALMA_RELEASE}" "${arch}"
  target_init "${id}" "${ALMA_MICRO_BASE_IMAGE}"
  target_bind_root_to_system_build "${id}"
  system_run_install_packages "${id}" "alternatives" "grep" "sed" "hostname"
  system_run_install_openjdk "${id}" "${openjdk_version}" "${openjdk_variant}"
  target_set_java_home "${id}" "${openjdk_version}"
  system_run_useradd "${id}" "appuser"
  target_set_user_and_workdir_to_home "${id}" "appuser"
  target_set_cmd_bash "${id}"
  target_commit "${id}" "${image_name}"
}

function build_image_zulu_openjdk_micro() {
  local arch="${1:?"Require architecture as first parameter ('amd64' or 'arm64')!"}"
  local openjdk_variant="${2:?"Require OpenJDK variant as second parameter ('jdk' or 'jre')!"}"
  local openjdk_version="${3:?"Require OpenJDK version as third parameter (e.g. 21.0.5)!"}"
  local image_name="${4:?"Require container image name as fourth parameter (e.g. zulu-openjdk-micro:21)!"}"

  local id
  id="${openjdk_version}-${openjdk_variant}-${arch}-$(date +%s)"

  set +e # do not abort on errors
  _build_image_zulu_openjdk_micro "${id}" "$@"
  local return_code=$?
  cleanup "${id}"
  set -e # abort on errors again
  return ${return_code}
}
