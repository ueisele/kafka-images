#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
source "${SCRIPT_DIR}/../env"
source "${SCRIPT_DIR}/build-openjdk-lib.sh"

PUSH=false
BUILD=false

CONTAINERREGISTRY=docker.io
CONTAINERREGISTRYUSER=ueisele

ARCHS=(amd64)

ZULU_OPENJDK_RELEASE=21

function usage() {
  echo "$0: $1" >&2
  echo
  echo "Usage: $0 [--build] [--push] [--registry docker.io] [--user ueisele] [--archs amd64,arm64] [--openjdk-release 21] [--openjdk-version 21.0.5]"
  echo
  return 1
}

function build() {
  for variant in "${ZULU_OPENJDK_VARIANTS[@]}"; do
    local image
    image="$(openjdk_micro_image_name "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}")"
    local tags=()
    IFS=" " read -r -a tags <<< "$(openjdk_image_tags "${variant}" "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")"
    for arch in "${ARCHS[@]}"; do
      echo "Building Container images for Zulu OpenJDK ${ZULU_OPENJDK_VERSION} with AlmaLinux ${ALMA_VERSION} for architecture ${arch}."
      local suffix
      if [ ${#ARCHS[@]} -gt 1 ]; then
        suffix="-${arch}"
      fi    
      local image_fqn="${image}${suffix}:${tags[0]}"
      build_image_zulu_openjdk_micro "${arch}" "${variant}" "${ZULU_OPENJDK_VERSION}" "${image_fqn}"
    done
  done
}

function push() {
  echo "Pushing Container images for Zulu OpenJDK ${ZULU_OPENJDK_VERSION} with AlmaLinux ${ALMA_VERSION}."
  local timestamp
  timestamp=$(date +%Y%m%d%H%M)
  local image
  image="$(openjdk_micro_image_name "${CONTAINERREGISTRY}" "${CONTAINERREGISTRYUSER}")"
  for variant in "${ZULU_OPENJDK_VARIANTS[@]}"; do
    local tags=()
    IFS=" " read -r -a tags <<< "$(openjdk_image_tags "${variant}" "${ZULU_OPENJDK_RELEASE}" "${ZULU_OPENJDK_VERSION}")"
    tags+=("${tags[0]}-${timestamp}")
    local manifest_fqn="${image}:${tags[0]}"
    if [ ${#ARCHS[@]} -gt 1 ]; then
      buildah manifest rm "${manifest_fqn}" 1>/dev/null 2>&1 || true
      buildah rmi "${manifest_fqn}" 1>/dev/null 2>&1 || true
      buildah manifest create "${manifest_fqn}"
      for arch in "${ARCHS[@]}"; do
        local image_fqn="${image}-${arch}:${tags[0]}"
        buildah manifest add "${manifest_fqn}" "${image_fqn}"
      done
      for tag in "${tags[@]}"; do
        buildah manifest push --all "${manifest_fqn}" "docker://${image}:${tag}"
      done
    else
      for tag in "${tags[@]}"; do
        buildah push --all "${manifest_fqn}" "docker://${image}:${tag}"
      done
    fi
  done
}

function parseCmd() {
  while [[ $# -gt 0 ]]; do
    case "$1" in
    --build)
      BUILD=true
      shift
      ;;
    --push)
      PUSH=true
      shift
      ;;
    --registry)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Container registry name"
        return 1
        ;;
      *)
        CONTAINERREGISTRY="$1"
        shift
        ;;
      esac
      ;;
    --user)
      shift
      case "$1" in
      "" | --*)
        usage "Requires Container registry user name"
        return 1
        ;;
      *)
        CONTAINERREGISTRYUSER="$1"
        shift
        ;;
      esac
      ;;
    --archs)
      shift
      case "$1" in
      "" | --*)
        usage "Requires comma-separated string of archs"
        return 1
        ;;
      *)
        IFS=, read -a ARCHS <<<"$1"
        shift
        ;;
      esac
      ;;
    --openjdk-release)
      shift
      case "$1" in
      "" | --*)
        usage "Requires OpenJDK release"
        return 1
        ;;
      *)
        ZULU_OPENJDK_RELEASE="$1"
        shift
        ;;
      esac
      ;;
    --openjdk-version)
      shift
      case "$1" in
      "" | --*)
        usage "Requires OpenJDK version"
        return 1
        ;;
      *)
        ZULU_OPENJDK_VERSION="$1"
        shift
        ;;
      esac
      ;;
    *)
      usage "Unknown option: $1"
      return $?
      ;;
    esac
  done

  if [ -z "${ZULU_OPENJDK_VERSION}" ]; then
    ZULU_OPENJDK_VERSION="$(openjdk_version_by_release "${ZULU_OPENJDK_RELEASE}")"
    if [ -z "${ZULU_OPENJDK_VERSION}" ]; then
      usage "requires OpenJDK version"
      return 1
    fi
  fi

  return 0
}

function main() {
  parseCmd "$@"
  local retval=$?
  if [ $retval != 0 ]; then
    exit $retval
  fi

  if [ "$BUILD" = true ]; then
    build
  fi
  if [ "$PUSH" = true ]; then
    push
  fi
}

main "$@"
